/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.PiClassExpressionConverterImpl2;
import it.unife.ml.srlframe.core.ClauseFactory;
import it.unife.ml.srlframe.core.Quantifier;
import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.VariableGenerator;
import it.unife.ml.srlframe.core.piclauses.AbstractPiClause;
import it.unife.ml.srlframe.core.piclauses.Conjunction;
import it.unife.ml.srlframe.core.piclauses.ClauseFactoryImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import static java.util.Arrays.asList;
/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
@RunWith(Parameterized.class)
public class PiClassExpressionConverterImpl2Test {

    private OWLClassExpression owlce;

    private AbstractPiClause expResult;

    private VariableGenerator varGenActual;

    private VariableGenerator varGenExpected;

    private static OWLOntology ontology;
    
    private static OWLDataFactory factory;
    
    private static ClauseFactory clauseFactory = new ClauseFactoryImpl();

    public PiClassExpressionConverterImpl2Test(OWLClassExpression owlce, AbstractPiClause expResult) {
        this.owlce = owlce;
        this.expResult = expResult;
    }

    @BeforeClass
    public static void setUpClass() throws OWLOntologyCreationException {
        ontology = OWLManager.createConcurrentOWLOntologyManager().createOntology();
        factory = ontology.getOWLOntologyManager().getOWLDataFactory();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        varGenActual = new VariableGenerator();
        varGenExpected = new VariableGenerator();
    }

    @Parameters
    public static Collection<Object[]> data() {
        OWLClassExpression classExpression;
        PiClause clause;
        Map<OWLClassExpression, PiClause> map = new HashMap<>();
        // Create the class expressions to test and the expected results
        // TO DO
        classExpression = factory.getOWLThing();
        clause = clauseFactory.getTop();
        map.put(classExpression, clause);
        
        classExpression = factory.getOWLNothing();
        clause = clauseFactory.getBottom();
        map.put(classExpression, clause);
        
        // (\exists R.(D or E)) and F
        OWLClass d = factory.getOWLClass(IRI.create("D"));
        OWLClass e = factory.getOWLClass(IRI.create("E"));
        OWLObjectUnionOf f1 = factory.getOWLObjectUnionOf(d, e);
        OWLObjectProperty r = factory.getOWLObjectProperty(IRI.create("R"));
        OWLObjectSomeValuesFrom f2 = factory.getOWLObjectSomeValuesFrom(r, f1);
        OWLClass f = factory.getOWLClass(IRI.create("F"));
        classExpression = factory.getOWLObjectIntersectionOf(f2, f);
        VariableGenerator varGen = new VariableGenerator();
        String xVar = varGen.next();
        String yVar = varGen.next();
        List<Quantifier> quantifiers = Collections.singletonList(new Quantifier(Quantifier.Type.EXISTS, yVar));
        PiClause orClause = clauseFactory.getDisjunction(clauseFactory.getPredicate("D", yVar), clauseFactory.getPredicate("E", yVar));
        clause = clauseFactory.getConjuntion(quantifiers, 
                clauseFactory.getPredicate(R, xVar, yVar),
                clauseFactory.getPredicate("D", y));
        
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        map.put(classExpression, clause);
        
        
        List<Object[]> data = new ArrayList<>();
        for (Entry entry : map.entrySet()){
            data.add(new Object[] {entry.getKey(), entry.getValue()});
        }
        
        return data;
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFOLFormula method, of class PiClassExpressionConverterImpl2.
     */
//    @Test
    public void testGetFOLFormula() {
        System.out.println("getFOLFormula");
        PiClassExpressionConverterImpl2 instance = new PiClassExpressionConverterImpl2(ontology, varGenActual);
        PiClause result = instance.getFOLFormula(owlce);
        assertEquals(expResult, result);
    }

    /**
     * Test of setCurrentVariable method, of class PiClassExpressionConverterImpl2.
     */
    @Test
    public void testSetCurrentVariable() {
        System.out.println("setCurrentVariable");
        Quantifier currentVariable = null;
        PiClassExpressionConverterImpl2 instance = null;
        instance.setCurrentVariable(currentVariable);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
