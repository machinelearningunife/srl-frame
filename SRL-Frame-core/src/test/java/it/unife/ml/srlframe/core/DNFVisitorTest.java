/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.DNFVisitor;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLProperty;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class DNFVisitorTest {
    
    public DNFVisitorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDNF method, of class DNFVisitor.
     */
    @Test
    public void testGetDNF() throws OWLOntologyCreationException {
        System.out.println("getDNF");
        OWLOntology onto = OWLManager.createOWLOntologyManager().createOntology();
        OWLDataFactory factory = onto.getOWLOntologyManager().getOWLDataFactory();
        OWLClass d = factory.getOWLClass(IRI.create("D"));
        OWLClass e = factory.getOWLClass(IRI.create("E"));
        OWLObjectUnionOf f1 = factory.getOWLObjectUnionOf(d, e);
        OWLObjectProperty r = factory.getOWLObjectProperty(IRI.create("R"));
        OWLObjectSomeValuesFrom f2 = factory.getOWLObjectSomeValuesFrom(r, f1);
        OWLClass f = factory.getOWLClass(IRI.create("F"));
        OWLObjectIntersectionOf f3 = factory.getOWLObjectIntersectionOf(f2, f);
        OWLClassExpression result = DNFVisitor.getDNF(onto, f3);
        Set<OWLClassExpression> expected = f3.asDisjunctSet();
    }
    
}
