/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.log;

import it.unife.ml.srlframe.core.log.LogBanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LogBannerTest {
    
    public LogBannerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of start method, of class LogBanner.
     */
    @Test
    public void test() {
        System.out.println(LogBanner.start("Even"));
        System.out.println(LogBanner.end());
        System.out.println(LogBanner.start("Odd"));
        System.out.println(LogBanner.end());
    }   
}
