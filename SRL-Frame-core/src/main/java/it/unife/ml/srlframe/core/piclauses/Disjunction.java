/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.Quantifier;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Disjunction extends ConnectiveImpl {

    public Disjunction(List<PiClause> clauses) {
        super(clauses);
    }

    public Disjunction(PiClause... clauses) {
        this(asList(clauses));
    }

    public Disjunction(List<Quantifier> quantifiers, List<PiClause> clauses) {
        super(quantifiers, clauses);
    }
//    @Override
//    public String getType() {
//        return "OR";
//    }

 
}
