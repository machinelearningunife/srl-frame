/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import java.util.Properties;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class SRLFrameProperties extends Properties {

    public static final String SRLFRAME = "SRL-Frame";

    private static SRLFrameProperties instance;

    private SRLFrameProperties() {

    }

    public static synchronized SRLFrameProperties getInstance() {
        if (instance == null) {
            instance = new SRLFrameProperties();
        }
        return instance;
    }

}
