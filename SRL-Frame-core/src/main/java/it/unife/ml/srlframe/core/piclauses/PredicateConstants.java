/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public enum PredicateConstants {
    
    SIMPLE(""),
    EQUALITY("="),
    INEQUALITY("!="),
    MAX_EXCLUSIVE(">"),
    MAX_INCLUSIVE(">="),
    MIN_EXCLUSIVE("<"),
    MIN_INCLUSIVE("<=")
    ;

    private final String text;

    /**
     * @param text
     */
    private PredicateConstants(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }
 
    
}
