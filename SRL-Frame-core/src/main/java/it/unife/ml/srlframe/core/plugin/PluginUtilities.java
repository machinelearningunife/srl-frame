/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.plugin;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class PluginUtilities {

    private final Logger logger = LoggerFactory.getLogger(PluginUtilities.class.getName());

    private static PluginUtilities instance;

    private BundleContext context;

    private PluginUtilities() {

    }

    public BundleContext getApplicationContext() {
        return context;
    }

    public Bundle getApplicationBundle() {
        return context.getBundle();
    }

    /**
     * Gets the one and only instance of <code>PluginUtilities</code>.
     */
    public static synchronized PluginUtilities getInstance() {
        if (instance == null) {
            instance = new PluginUtilities();
        }
        return instance;
    }

    /**
     * This method is called by the system to initialise the plugin utilities. Users should
     * <b>not</b> call this method.
     */
    public void initialise(BundleContext context) {
        this.context = context;
    }

    public static Version getBundleVersion(Bundle b) {
        return new Version((String) b.getHeaders().get("Bundle-Version"));
    }
}
