/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.Connective;
import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.Quantifier;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ConnectiveImpl extends AbstractPiClause implements Connective {

    protected List<PiClause> clauses;

    public ConnectiveImpl(List<PiClause> clauses) {
        super(Collections.emptyList());
        this.clauses = clauses;
    }

    public ConnectiveImpl(AbstractPiClause... clauses) {
        this(asList(clauses));
    }

    public ConnectiveImpl(List<Quantifier> quantifiers, List<PiClause> clauses) {
        super(quantifiers);
        this.clauses = clauses;
    }

    /**
     * @return the predicates
     */
    public List<PiClause> getClauses() {
        return clauses;
    }

    @Override
    public boolean equals(Object other) {
        ConnectiveImpl o;
        try {
            o = (ConnectiveImpl) other;
        } catch (ClassCastException cce) {
            return false;
        }
        // check same type
        if (this.getClass().equals(o.getClass())) {

            // check quantifiers
            if (quantifiers.size() == o.quantifiers.size()) {
                // the order of the quantifiers is important
                for (int i = 0; i < quantifiers.size(); i++) {
                    if (!quantifiers.get(i).equals(o.quantifiers.get(i))) {
                        return false;
                    }
                }
            } else {
                return false;
            }

            // check clauses
            if (this.clauses.size() == o.clauses.size()) {
                return o.clauses.containsAll(clauses);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
