/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import static it.unife.ml.srlframe.utils.GeneralUtils.safe;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface PiClause {
    public List<Quantifier> getQuantifiers();

    /**
     * @param quantifiers the quantifiers to set
     */
//    public void setQuantifiers(List<Quantifier> quantifiers) {
//        this.quantifiers = quantifiers;
//    }
   

    public boolean equals(Object other);
    
}
