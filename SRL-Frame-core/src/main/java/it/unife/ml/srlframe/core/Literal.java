/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Literal implements Predicate {

    private boolean positive = true; // Assume positive by default.
    //
    private String cachedStringRep = null;

    public Literal(String name, List<String> variables) {
        super(name, variables);
    }

    public Literal(String name, List<String> variables, boolean positive) {
        super(name, variables);
        this.positive = positive;
    }

    /**
     *
     * @return true if a positive literal, false otherwise.
     */
    public boolean isPositiveLiteral() {
        return positive;
    }

    /**
     *
     * @return true if a negative literal, false otherwise.
     */
    public boolean isNegativeLiteral() {
        return !positive;
    }

//	@Override
//	public String toString() {
//		if (null == cachedStringRep) {
//			StringBuilder sb = new StringBuilder();
//			if (isNegativeLiteral()) {
//				sb.append(Connective.NOT.toString());
//			}
//			sb.append(getAtomicSentence().toString());
//			cachedStringRep = sb.toString();
//		}
//
//		return cachedStringRep;
//	}
//	@Override
//	public boolean equals(Object o) {
//		if (this == o) {
//			return true;
//		}
//		if (!(o instanceof Literal)) {
//			return false;
//		}
//		Literal l = (Literal) o;
//		return l.isPositiveLiteral() == isPositiveLiteral()
//				&& l.getAtomicSentence().equals(getAtomicSentence());
//	}
}
