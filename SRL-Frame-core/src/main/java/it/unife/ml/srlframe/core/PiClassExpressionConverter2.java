/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.piclauses.AbstractPiClause;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLClassExpressionVisitor;
import org.semanticweb.owlapi.model.OWLPropertyExpressionVisitor;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface PiClassExpressionConverter2 extends OWLClassExpressionVisitor, OWLPropertyExpressionVisitor {
    
    public void setCurrentVariable(Quantifier currentVariable);
    
//    public ProbabilisticGeneralRule getProbabilisticGeneralRule();

    public PiClause getFOLFormula(OWLClassExpression owlce);
    
}