/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.owlkraider;

import it.unife.ml.kraider.api.Origin;
import it.unife.ml.kraider.api.TripleExtractor;
import it.unife.ml.ld.Axiom;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ExtractedAxiomStatistics {
    
    private final Logger logger = LoggerFactory.getLogger(ExtractedAxiomStatistics.class);

    /**
     * Count the number of extracted axioms
     */
    protected int nAxioms = 0;


    /**
     * keep count of how many triples where extracted from a domain
     */
    protected Map<URL, Integer> domainCount = new HashMap<>();   
    
    
    public synchronized void updateStatistics(Set<Axiom> axioms) {
        axioms.forEach(a -> {
            this.nAxioms++;
            URL domain = a.getOrigin().getDomain();
            getDomainCount().merge(domain, 1, Integer::sum);
        });
    }

    /**
     * @return the domainCount
     */
    public Map<URL, Integer> getDomainCount() {
        return domainCount;
    }

    /**
     * @return the nAxioms
     */
    public int getnAxioms() {
        return nAxioms;
    }
    
}
