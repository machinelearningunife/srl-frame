/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.owlkraider;

import it.unife.ml.probowlapi.constants.UniFeIRI;
import it.unife.ml.kraider.api.KRaiderConfiguration;
import it.unife.ml.kraider.api.KRaiderConfigurationBuilder;
import it.unife.ml.kraider.api.KnowledgeRaider;
import it.unife.ml.kraider.api.Triple;
import it.unife.ml.kraider.api.TripleStatistics;
import it.unife.ml.ld.Axiom;
import it.unife.ml.ld.queue.QueueCleaner;
import it.unife.ml.ld.queue.QueueCleanerConfiguration;
import it.unife.ml.ld.queue.QueueCleanerConfigurationBuilder;
import it.unife.ml.srlframe.core.framework.SRLFrameExecutable;
import it.unife.ml.srlframe.core.owl.OWLAPIOntologyCollector;
import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.util.tracker.ServiceTracker;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class OWLKRaiderExecutable implements SRLFrameExecutable {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(OWLKRaiderExecutable.class);

    // Bundle's context.
    private BundleContext bundleContext = null;
    // The service tacker object.
    private ServiceTracker serviceTracker = null;

    private KnowledgeRaider kraider = null;

    private String resource;

    private int recursionDepth;

    private QueueCleanerConfiguration configuration;

    private boolean stop;

    private OWLAPIOntologyCollector oWLAPIOntologyCollector;

    private OWLDataFactory df;

    private final String USAGE_STRING = "Usage owlkraider <resource> <recursionDepth>";

    OWLAnnotationProperty originDomainAnnotation;

//    public OWLKRaiderExecutable(BundleContext bundleContext,
//            String resource,
//            QueueCleanerConfiguration configuration) throws InvalidSyntaxException {
//        this.bundleContext = bundleContext;
//        this.resource = resource;
//        this.configuration = configuration;
//        // check if service is registered 
//        // Create a service tracker to monitor services.
//        serviceTracker = new ServiceTracker(
//                bundleContext,
//                bundleContext.createFilter(
//                        "(&(objectClass=" + KnowledgeRaider.class.getName() + "))"),
//                null);
//        serviceTracker.open();
//
//        // Get the kraider service, if available.
//        kraider = (KnowledgeRaider) serviceTracker.getService();
//
//        this.oWLAPIOntologyCollector = new OWLAPIOntologyCollector();
//        this.df = oWLAPIOntologyCollector.getFactory();
//        originDomainAnnotation = df.getOWLAnnotationProperty(IRI.create(UniFeIRI.ORIGIN_DOMAIN));
//    }
    public OWLKRaiderExecutable(BundleContext bundleContext, List<String> args) throws InvalidSyntaxException, IllegalArgumentException {
        this.bundleContext = bundleContext;
        if (args.size() < 2) {
            throw new IllegalArgumentException(USAGE_STRING);
        }
        resource = args.get(0);
        try {
            recursionDepth = Integer.parseInt(args.get(1));
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(args.get(1) + " is not a number.\n" + USAGE_STRING);
        }
        QueueCleanerConfigurationBuilder confBuilder = new QueueCleanerConfigurationBuilder();
        configuration = confBuilder.buildConfiguration();
        // check if service is registered 
        // Create a service tracker to monitor services.
        serviceTracker = new ServiceTracker(
                bundleContext,
                bundleContext.createFilter(
                        "(&(objectClass=" + KnowledgeRaider.class.getName() + "))"),
                null);
        serviceTracker.open();

        // Get the kraider service, if available.
        kraider = (KnowledgeRaider) serviceTracker.getService();

        this.oWLAPIOntologyCollector = new OWLAPIOntologyCollector();
        this.df = oWLAPIOntologyCollector.getFactory();
        originDomainAnnotation = df.getOWLAnnotationProperty(IRI.create(UniFeIRI.ORIGIN_DOMAIN));
    }

    @Override
    public void execute() {

        // used for stopping the thread when the timeout is reached
        Thread timerChecker = new Thread(new TimerChecker(configuration.getTimeout()));
        timerChecker.start();
        long startTime = System.currentTimeMillis();
        QueueCleaner queueCleaner = null;
        try {
            // start raiding
            KRaiderConfigurationBuilder kraiderConfBuilder = new KRaiderConfigurationBuilder(kraider.getDefaultConfiguration(resource));
            KRaiderConfiguration kraiderConf = kraiderConfBuilder.recursionDepth(recursionDepth).buildConfiguration();
            BlockingQueue<Triple> tripleQueue = kraider.raidKnowledge(kraiderConf, false);

            TripleStatistics tripleStatistics = kraider.getExtractedTriplesStatistics();
            ExtractedAxiomStatistics axiomStatistics = new ExtractedAxiomStatistics();

            // instatiate queueCleaner
            queueCleaner = new QueueCleaner(tripleQueue, configuration);
            queueCleaner.addListener((e) -> {
                Set<Axiom> axioms = (Set<Axiom>) e.getSource();
                axiomStatistics.updateStatistics(axioms);
            });

            // start conversion into Axiom
            BlockingQueue<Axiom> axiomQueue = queueCleaner.startConversion();

            // convert axioms into annotated axioms
            while (!stopCriteria()) {
                Axiom axiom = axiomQueue.poll(configuration.getTriplePollTimeout(), TimeUnit.SECONDS);
                if (axiom != null) {
                    OWLAxiom owlAxiom = convertIntoAnnotatedAxiom(axiom);
                    oWLAPIOntologyCollector.addAxiom(owlAxiom);
                } else {
                    stop = true;
                }
            }

            long estimatedTime = System.currentTimeMillis() - startTime;

            // print statistics
            printTripleStatistics(tripleStatistics);
            printAxiomStatistics(axiomStatistics);
            logger.info("Total Time: {} s", estimatedTime / 1000.0);
            logger.info("");

            // save the Ontology
            oWLAPIOntologyCollector.saveOntology();
            logger.info("Ontology saved in: " + oWLAPIOntologyCollector.getOntologyPath());

        } catch (MalformedURLException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        } catch (InterruptedException ex) {
            logger.error(ex.getLocalizedMessage());
            throw new RuntimeException(ex);
        } finally {
            if (queueCleaner != null) {
                queueCleaner.stop();
            }
            kraider.stop();

        }
    }

    private boolean stopCriteria() {
        return stop;
    }

    private OWLAxiom convertIntoAnnotatedAxiom(Axiom axiom) {
        Set<OWLAnnotation> annotations = new HashSet<>();

        OWLLiteral literal = df.getOWLLiteral(axiom.getOrigin().getDomain().toString());
        OWLAnnotation originDomainAnn = df.getOWLAnnotation(originDomainAnnotation, literal);
        annotations.add(originDomainAnn);
        return axiom.getAxiom().getAnnotatedAxiom(annotations);
    }

    private void printTripleStatistics(TripleStatistics tripleStatistics) {
        logger.info("");
        logger.info("============ Triple Extraction Result ============");
        logger.info("Extracted triples: {}", tripleStatistics.getnTriples());
        tripleStatistics.getMethodCount().forEach((k, v) -> {
            logger.info("Method: {}\tN. Triples: {}", k.getCanonicalName(), v);
        });
        tripleStatistics.getDomainCount().forEach((k, v) -> {
            logger.info("Domain: {}\tN. Triples: {}", k, v);
        });
        tripleStatistics.getOriginCount().forEach((k, v) -> {
            logger.info("Domain: {}\tMethod: {}\tN. Triples: {}", k.getDomain(), k.getMethod().getCanonicalName(), v);
        });

        logger.info("");
        logger.info("================================");
        logger.info("");
    }

    private void printAxiomStatistics(ExtractedAxiomStatistics axiomStatistics) {
        logger.info("");
        logger.info("============ Axiom Extraction Result ============");
        logger.info("Extracted axioms: {}", axiomStatistics.getnAxioms());
        axiomStatistics.getDomainCount().forEach((k, v) -> {
            logger.info("Domain: {}\tN. Axioms: {}", k, v);
        });

        logger.info("");
        logger.info("================================");
        logger.info("");
    }

    private class TimerChecker implements Runnable {

        private final long timeout;

        TimeUnit timeUnit = TimeUnit.SECONDS;

        public TimerChecker(long timeout, TimeUnit timeUnit) {
            this.timeout = timeout;
            this.timeUnit = timeUnit;
        }

        public TimerChecker(long timeout) {
            this.timeout = timeout;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(timeout);
                stop = true;

            } catch (InterruptedException ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                //Logger.getLogger(PelletExplain.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

}
