/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import java.util.Iterator;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
class VariableGenerator implements Iterator<String> {

    private int now = -1;
    private String prefix = "";
    private static char[] vs;

    static {
        vs = new char['Z' - 'A' + 1];
        for (char i = 'A'; i <= 'Z'; i++) {
            vs[i - 'A'] = i;
        }
    }

    private String fixPrefix(String prefix) {
        if (prefix.length() == 0) {
            return Character.toString(vs[0]);
        }
        int last = prefix.length() - 1;
        char next = (char) (prefix.charAt(last) + 1);
        String sprefix = prefix.substring(0, last);
        return next - vs[0] == vs.length
                ? fixPrefix(sprefix) + vs[0] : sprefix + next;
    }

//    @Override
//    protected String computeNext() {
//        if (++now == vs.length) {
//            prefix = fixPrefix(prefix);
//        }
//        now %= vs.length;
//        return new StringBuilder().append(prefix).append(vs[now]).toString();
//    }
    /**
     * Returns the next element in the iteration without advancing the iteration.
     */
    protected String peek() {
        int temp = now;
        if (++temp == vs.length) {
            prefix = fixPrefix(prefix);
        }
        temp %= vs.length;
        return new StringBuilder().append(prefix).append(vs[temp]).toString();
    }

    public String getCurrentVariable() {
        return new StringBuilder().append(prefix).append(vs[now]).toString();
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public String next() {
        String result = peek();
        now++;
        return result;
    }
}
