/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.piclauses.PredicateImpl;
import it.unife.ml.srlframe.core.piclauses.Negation;
import it.unife.ml.srlframe.core.piclauses.Conjunction;
import it.unife.ml.srlframe.core.piclauses.Disjunction;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface PiVisitor {
    
    public void visit(Conjunction conj);
    
    public void visit(Disjunction disj);
    
    public void visit(Negation neg);
      
    public void visit(PredicateImpl pred);
    
}
