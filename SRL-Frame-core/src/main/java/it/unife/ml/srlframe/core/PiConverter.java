/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import org.semanticweb.owlapi.model.OWLOntology;

/**
 * Class used to convert an OWL/RDF to a Logic Program
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface PiConverter {

    public ProbabilisticLogicProgram getProbabilisticLogicProgram();

    public void visit(OWLOntology ontology);
}
