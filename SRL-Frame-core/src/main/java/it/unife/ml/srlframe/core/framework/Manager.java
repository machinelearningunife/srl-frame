/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.framework;

import it.unife.ml.srlframe.core.owlkraider.OWLKRaiderExecutable;
import it.unife.ml.ld.queue.QueueCleanerConfiguration;
import it.unife.ml.ld.queue.QueueCleanerConfigurationBuilder;
import java.util.List;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Manager {
    
    private static final Logger logger = LoggerFactory.getLogger(OWLKRaiderExecutable.class);
    
    private BundleContext bundleContext;
    
    public Manager(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
    }
    
    public SRLFrameExecutable getExecutable(String name, List<String> args) {
        // TO DO: use Eclipse plugin approach in the future.

        if (name.equals("owlkraider")) {

            try {
                return new OWLKRaiderExecutable(bundleContext, args);
            } catch (InvalidSyntaxException ise) {
                logger.error(ise.getLocalizedMessage());
                return null;   
            } catch (IllegalArgumentException iae) {
                logger.error(iae.getLocalizedMessage());
                return null;   
            }
        } else {
            return null;
        }
    }
    
}
