/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.ClauseFactory;
import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.Predicate;
import it.unife.ml.srlframe.core.Quantifier;
import static it.unife.ml.srlframe.core.piclauses.PredicateConstants.*;
import java.util.Collections;
import java.util.List;
import org.semarglproject.vocab.OWL;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ClauseFactoryImpl implements ClauseFactory {

    @Override
    public Predicate getTop() {
        return new PredicateImpl(SIMPLE, OWL.THING, Collections.emptyList());
    }

    @Override
    public Predicate getBottom() {
        return new PredicateImpl(SIMPLE, OWL.NOTHING, Collections.emptyList());
    }

    @Override
    public Conjunction getConjuntion(List<Quantifier> quantifiers, List<PiClause> clauses) {
        return new Conjunction(quantifiers, clauses);
    }

    @Override
    public Conjunction getConjuntion(List<PiClause> clauses) {
        return new Conjunction(clauses);
    }

    @Override
    public Disjunction getDisjunction(List<Quantifier> quantifiers, List<PiClause> clauses) {
        return new Disjunction(quantifiers, clauses);
    }

    @Override
    public Disjunction getDisjunction(List<PiClause> clauses) {
        return new Disjunction(clauses);
    }

    @Override
    public Negation getNegation(PiClause... clauses) {
        return new Negation(clauses);
    }

    @Override
    public Negation getNegation(List<PiClause> clauses) {
        return new Negation(clauses);
    }

    @Override
    public Predicate getEquality(String var1, String var2) {
        return new PredicateImpl(EQUALITY, EQUALITY.toString(), var1, var2);
    }

    @Override
    public Predicate getInequality(String var1, String var2) {
        return new PredicateImpl(INEQUALITY, INEQUALITY.toString(), var1, var2);
    }

    @Override
    public Predicate getMinExclusive(String var1, String var2) {
        return new PredicateImpl(MIN_EXCLUSIVE, MIN_EXCLUSIVE.toString(), var1, var2);
    }
    
    @Override
    public Predicate getMinInclusive(String var1, String var2) {
        return new PredicateImpl(MIN_INCLUSIVE, MIN_INCLUSIVE.toString(), var1, var2);
    }

    @Override
    public Predicate getMaxExclusive(String var1, String var2) {
        return new PredicateImpl(MAX_EXCLUSIVE, MAX_EXCLUSIVE.toString(), var1, var2);
    }

    @Override
    public Predicate getMaxInclusive(String var1, String var2) {
        return new PredicateImpl(MAX_INCLUSIVE, MAX_EXCLUSIVE.toString(), var1, var2);
    }

    @Override
    public Predicate getPredicate(List<Quantifier> quantifiers, String id, List<String> variables) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Predicate getPredicate(String id, List<String> variables) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Predicate getPredicate(List<Quantifier> quantifiers, String id, String... variables) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Predicate getPredicate(String id, String... variables) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Negation getNegation(List<Quantifier> quantifiers, PiClause... clauses) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Negation getNegation(List<Quantifier> quantifiers, List<PiClause> clauses) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
