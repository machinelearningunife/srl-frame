/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.utils;

import java.util.Locale;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class InfoUtilities {

    public static String getCountry() {
        return Locale.getDefault().getCountry();
    }

    public static String getLang() {
        return Locale.getDefault().getLanguage();
    }

    public static long getMaxMemoryInMegaBytes() {
        return (Runtime.getRuntime().maxMemory() / 1000000);
    }
    
}
