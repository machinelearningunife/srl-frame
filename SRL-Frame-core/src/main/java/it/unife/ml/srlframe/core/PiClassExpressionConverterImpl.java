package it.unife.ml.srlframe.core;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//import java.util.Stack;
//import org.semanticweb.owlapi.model.OWLClass;
//import org.semanticweb.owlapi.model.OWLClassExpression;
//import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
//import org.semanticweb.owlapi.model.OWLDataExactCardinality;
//import org.semanticweb.owlapi.model.OWLDataFactory;
//import org.semanticweb.owlapi.model.OWLDataHasValue;
//import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
//import org.semanticweb.owlapi.model.OWLDataMinCardinality;
//import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
//import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
//import org.semanticweb.owlapi.model.OWLObjectComplementOf;
//import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
//import org.semanticweb.owlapi.model.OWLObjectHasSelf;
//import org.semanticweb.owlapi.model.OWLObjectHasValue;
//import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
//import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
//import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
//import org.semanticweb.owlapi.model.OWLObjectOneOf;
//import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
//import org.semanticweb.owlapi.model.OWLObjectUnionOf;
//import org.semanticweb.owlapi.model.OWLOntology;
//
///**
// *
// * @author Giuseppe Cota <giuseppe.cota@unife.it>
// */
//public class PiClassExpressionConverterImpl implements PiClassExpressionConverter {
//
//    private Stack<List<Literal>> stack = new Stack<>();
//
//    private VariableGenerator varGen;
//
//    private OWLDataFactory factory;
//
//    private OWLOntology ontology;
//
//    private Quantifier currVar;
//
//    public PiClassExpressionConverterImpl(OWLOntology ontology, VariableGenerator varGen) {
//        this.ontology = ontology;
//        this.factory = ontology.getOWLOntologyManager().getOWLDataFactory();
//        this.varGen = varGen;
//    }
//
//    @Override
//    public DisjunctionOfClauses getDisjunctionOfClauses(OWLClassExpression owlce) {
//        // convert to DNF 
//        OWLClassExpression owlceDNF = DNFVisitor.getDNF(ontology, owlce);
//        owlceDNF.accept(this);
//        return new DisjunctionOfClauses(stack.pop());
//    }
//
//    public DisjunctionOfClauses getConjunctionOfClauses() {
//        return new DisjunctionOfClauses(stack.pop());
//    }
//
//    public static DisjunctionOfClauses getConjunctionOfClauses(OWLOntology ontology, VariableGenerator varGen, OWLClassExpression owlce) {
//        // convert to DNF 
//        OWLClassExpression owlceDNF = DNFVisitor.getDNF(ontology, owlce);
//        PiClassExpressionConverterImpl piVisitor = new PiClassExpressionConverterImpl(ontology, varGen);
//        owlceDNF.accept(piVisitor);
//        return piVisitor.getConjunctionOfClauses();
//    }
//
//    @Override
//    public void setCurrentVariable(Quantifier currentVariable) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLClass owlc) {
//        Literal l = new Literal(owlc.toStringID(), Collections.singletonList(currVar.id));
//        stack.peek().add(l);
//    }
//
//    @Override
//    public void visit(OWLObjectIntersectionOf owloio) {
//        OWLClassExpression owlce1 = owloio.Connective co1 = stack.pop();
//        Connective co2 = stack.pop();
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectUnionOf owlouo) {
//        stack.push(new ArrayList<>());
//        for (OWLClassExpression ce : owlouo.getOperands()) {
//           ce.accept(this);
//        }
//        
//    }
//
//    @Override
//    public void visit(OWLObjectComplementOf owloco) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectSomeValuesFrom owlsvf) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectAllValuesFrom owlvf) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectHasValue owlohv) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectMinCardinality owlomc) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectExactCardinality owloec) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectMaxCardinality owlomc) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectHasSelf owlohs) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLObjectOneOf owlooo) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataSomeValuesFrom o) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataAllValuesFrom owldvf) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataHasValue owldhv) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataMinCardinality owldmc) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataExactCardinality owldec) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void visit(OWLDataMaxCardinality owldmc) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//}
