/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.piclauses.Disjunction;
import it.unife.ml.srlframe.core.piclauses.Conjunction;
import it.unife.ml.srlframe.core.piclauses.Negation;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface ClauseFactory {

    public Predicate getTop();

    public Predicate getBottom();

    public Conjunction getConjuntion(List<Quantifier> quantifiers, List<PiClause> clauses);

    public Conjunction getConjuntion(List<PiClause> clauses);

    public Disjunction getDisjunction(List<Quantifier> quantifiers, List<PiClause> clauses);

    public Disjunction getDisjunction(List<PiClause> clauses);
    
    public Predicate getPredicate(List<Quantifier> quantifiers, String id, List<String> variables);
    
    public Predicate getPredicate(String id, List<String> variables);
    
    public Predicate getPredicate(List<Quantifier> quantifiers, String id, String... variables);
    
    public Predicate getPredicate(String id, String... variables);

    public Negation getNegation(List<Quantifier> quantifiers, PiClause... clauses);
    
    public Negation getNegation(PiClause... clauses);

    public Negation getNegation(List<Quantifier> quantifiers, List<PiClause> clauses);
    
    public Negation getNegation(List<PiClause> clauses);

    public Predicate getEquality(String var1, String var2);

    public Predicate getInequality(String var1, String var2);

    public Predicate getMinExclusive(String var1, String var2);
    
    public Predicate getMinInclusive(String var1, String var2);
    
    public Predicate getMaxExclusive(String var1, String var2);
            
    public Predicate getMaxInclusive(String var1, String var2);
    
}
