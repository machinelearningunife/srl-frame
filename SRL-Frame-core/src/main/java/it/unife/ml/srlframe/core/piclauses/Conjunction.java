/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.Quantifier;
import static java.util.Arrays.asList;
import java.util.List;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Conjunction extends ConnectiveImpl {

    public Conjunction(List<PiClause> clauses) {
        super(clauses);
    }

    public Conjunction(PiClause... clauses) {
        this(asList(clauses));
    }
    
    public Conjunction(List<Quantifier> quantifiers, List<PiClause> clauses) {
        super(quantifiers, clauses);
    }

//    @Override
//    public String getType() {
//        return "AND";
//    }
}
