/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.PiClause;
import it.unife.ml.srlframe.core.PiVisitor;
import it.unife.ml.srlframe.core.Quantifier;
import static it.unife.ml.srlframe.utils.GeneralUtils.safe;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.List;
import static it.unife.ml.srlframe.utils.GeneralUtils.safe;
import static it.unife.ml.srlframe.utils.GeneralUtils.safe;
import static it.unife.ml.srlframe.utils.GeneralUtils.safe;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public abstract class AbstractPiClause implements PiClause{

    /**
     * @return the quantifiers
     */
    public List<Quantifier> getQuantifiers() {
        return safe(quantifiers);
    }

    /**
     * @param quantifiers the quantifiers to set
     */
//    public void setQuantifiers(List<Quantifier> quantifiers) {
//        this.quantifiers = quantifiers;
//    }
    
    protected List<Quantifier> quantifiers;
    
    
    public AbstractPiClause(List<Quantifier> quantifiers) {
        this.quantifiers = quantifiers;
    }

    public abstract boolean equals(Object other);
    
    public void accept(PiVisitor visitor) {
        
    }
    
//    public boolean isTop() {
//        return this.type == Type.TOP;
//    }
//    
//    public boolean isBottom() {
//        return this.type == Type.BOTTOM;
//    }
//    
//    public static AbstractPiClause getTop() {
//        return new AbstractPiClause(Collections.emptyList(), AbstractPiClause.Type.TOP);
//    } 
//    
//    public static AbstractPiClause getBottom() {
//        return new AbstractPiClause(Collections.emptyList(), AbstractPiClause.Type.BOTTOM);
//    }
    
}
