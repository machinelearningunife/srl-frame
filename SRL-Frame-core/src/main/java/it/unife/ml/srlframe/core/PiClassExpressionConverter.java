/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import org.semanticweb.owlapi.model.OWLClassExpressionVisitor;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public interface PiClassExpressionConverter extends OWLClassExpressionVisitor {
    
    public void setCurrentVariable(Quantifier currentVariable);
    
//    public ProbabilisticGeneralRule getProbabilisticGeneralRule();

//    public DisjunctionOfClauses getDisjunctionOfClauses(OWLClassExpression owlce);
    
}
