/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Quantifier {

    private String id;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the quantifier
     */
    public Type getQuantifier() {
        return quantifier;
    }

    public enum Type {
        EXISTS, ALL
    }

    private Type quantifier;

    public Quantifier(Type quantifier, String id) {
        this.quantifier = quantifier;
        this.id = id;
    }

}
