/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.piclauses;

import it.unife.ml.srlframe.core.Predicate;
import it.unife.ml.srlframe.core.Quantifier;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.List;
import org.semarglproject.vocab.OWL;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class PredicateImpl extends AbstractPiClause implements Predicate {
    
    private PredicateConstants type;

    private String id;

    private List<String> variables;

    public PredicateImpl(PredicateConstants type, List<Quantifier> quantifiers, String id, List<String> variables) {
        super(quantifiers);
        this.type = type;
        this.id = id;
        this.variables = variables;
    }

    public PredicateImpl(PredicateConstants type, String id, List<String> variables) {
        super(Collections.emptyList());
        this.type = type;
        this.id = id;
        this.variables = variables;
    }

    public PredicateImpl(PredicateConstants type, List<Quantifier> quantifiers, String id, String... variables) {
        this(type, quantifiers, id, asList(variables));
    }

    public PredicateImpl(PredicateConstants type, String id, String... variables) {
        this(type, id, asList(variables));
    }

    public boolean isTop() {
        return this.id.equals(OWL.THING);
    }

    public boolean isBottom() {
        return this.id.equals(OWL.NOTHING);
    }

    @Override
    public boolean equals(Object other) {
        PredicateImpl o;
        try {
            o = (PredicateImpl) other;
        } catch (ClassCastException cce) {
            return false;
        }
        // check same type
        if (this.getClass().equals(o.getClass())) {
            if (id.equals(o.id)) {
                if (variables.size() == o.variables.size()) {
                    return variables.containsAll(o.variables);
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the variables
     */
    public List<String> getVariables() {
        return variables;
    }

}
