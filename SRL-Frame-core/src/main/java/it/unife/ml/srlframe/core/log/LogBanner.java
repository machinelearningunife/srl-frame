/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.log;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LogBanner {

    private static final int WIDTH = 80;

    private static final String FOOTER;

    static {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < WIDTH; i++) {
            sb.append("-");
        }
        FOOTER = sb.toString();
    }


    public static String start(String title) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for( ; i < WIDTH / 2 - title.length() / 2 - 1; i++) {
            sb.append("-");
        }
        sb.append(" ");
        i++;
        sb.append(title);
        i += title.length();
        sb.append(" ");
        i++;
        for(; i < WIDTH; i++) {
            sb.append("-");
        }
        return sb.toString();
    }

    public static String end() {
        return "";
    }


    @Override
    public String toString() {
        return "";
    }
}