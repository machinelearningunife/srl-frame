/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.ml.srlframe.core.piclauses.AbstractPiClause;
import it.unife.ml.srlframe.core.piclauses.Disjunction;
import it.unife.ml.srlframe.core.piclauses.Conjunction;
import it.unife.ml.srlframe.core.piclauses.Negation;
import it.unife.ml.srlframe.core.piclauses.ClauseFactoryImpl;
import it.unife.ml.srlframe.core.piclauses.PredicateImpl;
import java.util.ArrayList;
import static java.util.Arrays.asList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataComplementOf;
import org.semanticweb.owlapi.model.OWLDataExactCardinality;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataHasValue;
import org.semanticweb.owlapi.model.OWLDataIntersectionOf;
import org.semanticweb.owlapi.model.OWLDataMaxCardinality;
import org.semanticweb.owlapi.model.OWLDataMinCardinality;
import org.semanticweb.owlapi.model.OWLDataOneOf;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataRange;
import org.semanticweb.owlapi.model.OWLDataRangeVisitor;
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLDataUnionOf;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeRestriction;
import org.semanticweb.owlapi.model.OWLFacetRestriction;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectComplementOf;
import org.semanticweb.owlapi.model.OWLObjectExactCardinality;
import org.semanticweb.owlapi.model.OWLObjectHasSelf;
import org.semanticweb.owlapi.model.OWLObjectHasValue;
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf;
import org.semanticweb.owlapi.model.OWLObjectInverseOf;
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality;
import org.semanticweb.owlapi.model.OWLObjectMinCardinality;
import org.semanticweb.owlapi.model.OWLObjectOneOf;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectUnionOf;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import org.semanticweb.owlapi.vocab.OWLFacet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class PiClassExpressionConverterImpl2 implements PiClassExpressionConverter2 {

    private final Logger logger = LoggerFactory.getLogger(PiClassExpressionConverterImpl2.class.getCanonicalName());

    private Stack<PiClause> stack = new Stack<>();

    private VariableGenerator varGen;

    private OWLDataFactory factory;

    private OWLOntology ontology;

    private String piVar;

    private String linkVar;

    private ClauseFactory clauseFactory = new ClauseFactoryImpl();

    public PiClassExpressionConverterImpl2(OWLOntology ontology, VariableGenerator varGen) {
        this.ontology = ontology;
        this.factory = ontology.getOWLOntologyManager().getOWLDataFactory();
        this.varGen = varGen;
        piVar = varGen.getCurrentVariable();
        linkVar = varGen.next();
    }

    @Override
    public PiClause getFOLFormula(OWLClassExpression owlce) {
        return stack.pop();
    }

    @Override
    public void setCurrentVariable(Quantifier currentVariable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLClass owlc) {
        PiClause atom;
        if (owlc.isOWLNothing()) {
            atom = clauseFactory.getBottom();
        } else if (owlc.isOWLThing()) {
            atom = clauseFactory.getTop();
        } else {
            atom = clauseFactory.getPredicate(owlc.toStringID(), Collections.singletonList(piVar));
//        Conjunction c = new Conjunction(Collections.singletonList(atom));
        }
        stack.push(atom);
    }

    @Override
    public void visit(OWLObjectIntersectionOf owloio) {
        Conjunction and = new Conjunction(Collections.emptyList());

        for (OWLClassExpression ce : owloio.getOperands()) {
            ce.accept(this);
            PiClause operand = stack.pop();
            if (operand instanceof Predicate) {
                if (((Predicate) operand).isBottom()) {
                    stack.push(operand);
                    return;  // exit from method
                } else if (!((Predicate) operand).isTop()) {
                    and.getClauses().add(operand);
                }
            } else if (operand instanceof Conjunction) {
                // I can flatten here
                and.getClauses().addAll(((Conjunction) operand).getClauses());
            } else {
                and.getClauses().add(operand);
            }
        }
        stack.push(and);
    }

    @Override
    public void visit(OWLObjectUnionOf owlouo) {
        Disjunction or = new Disjunction(Collections.emptyList());
        for (OWLClassExpression ce : owlouo.getOperands()) {
            ce.accept(this);
            PiClause operand = stack.pop();
            if (operand instanceof Predicate) {
                if (((Predicate) operand).isTop()) {
                    stack.push(operand);
                    return;  // exit from method
                } else if (!((Predicate) operand).isBottom()) {
                    or.getClauses().add(operand);
                }
            } else if (operand instanceof Disjunction) {
                or.getClauses().addAll(((Disjunction) operand).getClauses());
            } else {
                or.getClauses().add(operand);
            }
        }
        stack.push(or);
    }

    @Override
    public void visit(OWLObjectComplementOf owloco) {
        OWLClassExpression ce = owloco.getOperand();
        ce.accept(this);
        PiClause c = stack.pop();
        PiClause negation;
        if (c instanceof Predicate) {
            if (((Predicate) c).isBottom()) {
                negation = clauseFactory.getTop();
            } else if (((Predicate) c).isTop()) {
                negation = clauseFactory.getBottom();
            } else {
                negation = new Negation(Collections.singletonList(c));
            }
        } else {
            negation = new Negation(Collections.singletonList(c));
        }
        stack.push(negation);
    }

    @Override
    public void visit(OWLObjectSomeValuesFrom owlsvf) {
//        String varX = varGen.getCurrentVariable();
//        String varY = varGen.next();
        String varY = linkVar;
        Quantifier quant = new Quantifier(Quantifier.Type.EXISTS, varY);
        OWLObjectPropertyExpression prop = owlsvf.getProperty();
        prop.accept(this);
        piVar = varY;
        linkVar = varGen.next();
        Predicate pred;
        PiClause c = stack.pop();
        try {
            pred = (Predicate) c;
        } catch (ClassCastException cce) {
            logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                    Predicate.class.getCanonicalName(), c.getClass());
            throw cce;
        }
        OWLClassExpression ce = owlsvf.getFiller();
        ce.accept(this);
        PiClause filler = stack.pop();
        PiClause clause;
        if (filler instanceof Predicate) {
            if (((Predicate) filler).isBottom()) {
                // !!! È FALSE ???
//                clause = new Negation(pred);
                clause = clauseFactory.getBottom();
            } else if (((Predicate) filler).isTop()) {
                clause = clauseFactory.getPredicate(asList(quant), pred.getId(), pred.getVariables());
            } else {
                clause = clauseFactory.getConjuntion(asList(quant), asList(pred, filler));
            }
        } else {
            clause = clauseFactory.getConjuntion(asList(quant), asList(pred, filler));
        }
        stack.push(clause);
    }

    @Override
    public void visit(OWLObjectAllValuesFrom owlvf) {
        String varY = linkVar;
        Quantifier quant = new Quantifier(Quantifier.Type.ALL, varY);
        OWLObjectPropertyExpression prop = owlvf.getProperty();
        prop.accept(this);
        piVar = varY;
        linkVar = varGen.next();
        Predicate atom;
        PiClause c = stack.pop();
        try {
            atom = (Predicate) c;
        } catch (ClassCastException cce) {
            logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                    Predicate.class.getCanonicalName(), c.getClass());
            throw cce;
        }
        OWLClassExpression ce = owlvf.getFiller();
        ce.accept(this);
        PiClause filler = stack.pop();
        Negation not_atom = new Negation(atom);
        PiClause clause;
        if (filler instanceof Predicate) {
            if (((Predicate) filler).isBottom()) {
                clause = clauseFactory.getPredicate(asList(quant), atom.getId(), atom.getVariables());
            } else if (((Predicate) filler).isTop()) {
                clause = not_atom;
            } else {
                clause = clauseFactory.getDisjunction(asList(quant), asList(not_atom, filler));
            }
        } else {
            // converting \exists_Y r(X,Y) -> p(X) to \exists_Y ~r(X,Y) \/ p(X)
            clause = clauseFactory.getDisjunction(asList(quant), asList(not_atom, filler));
        }
        stack.push(clause);
    }

    /**
     * Converts ObjectHasValue (R , i) (or in DL Syntax \exists R.{i} into the FOL formula R(X,i);
     *
     * @param owlohs
     */
    @Override
    public void visit(OWLObjectHasValue owlohv) {
        OWLObjectPropertyExpression prop = owlohv.getProperty();
        prop.accept(this);
        PiClause c = stack.pop();
        Predicate p;
        try {
            p = (Predicate) c;
        } catch (ClassCastException cce) {
            logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                    Predicate.class.getCanonicalName(), c.getClass());
            throw cce;
        }
        OWLIndividual ind = owlohv.getFiller();
        p = clauseFactory.getPredicate(p.getId(), asList(piVar, ind.toStringID()));
        stack.push(p);
    }

    @Override
    public void visit(OWLObjectMinCardinality owlomc) {
        // TO DO
        // Da considerare il caso in cui il filler sia Thing

        OWLObjectPropertyExpression prop = owlomc.getProperty();
        String varX = piVar;
        int cardinality = owlomc.getCardinality();
        List<Quantifier> quantifiers = new ArrayList<>(cardinality);
        List<PiClause> clauses = new ArrayList<>(cardinality);
        List<Predicate> inequalities = new ArrayList<>();
        quantifiers.add(new Quantifier(Quantifier.Type.EXISTS, linkVar));
        for (int i = 0; i < cardinality; i++) {
            String varY = linkVar;

            prop.accept(this);
            PiClause c = stack.pop();
            Predicate p;
            try {
                p = (Predicate) c;
            } catch (ClassCastException cce) {
                logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                        Predicate.class.getCanonicalName(), c.getClass());
                throw cce;
            }

            piVar = linkVar;
            linkVar = varGen.next();
            quantifiers.add(new Quantifier(Quantifier.Type.EXISTS, linkVar));
            OWLClassExpression owlFiller = owlomc.getFiller();
            if (!owlFiller.isOWLNothing()) {
                clauses.add(p);
                if (!owlFiller.isOWLThing()) {
                    owlFiller.accept(this);
                    PiClause filler = stack.pop();
                    clauses.add(filler);
                }
            } else if (owlFiller.isOWLNothing()) {
                // !!! È FALSE ???
                
//                AbstractPiClause notProperty = new Negation(p);
//                clauses.add(notProperty);
                piVar = varX;
                stack.push(clauseFactory.getBottom());
                return;
            }
            piVar = varX;
        }
        for (int j = 1; j < cardinality; j++) {
            for (int i = 0; i < j; i++) {
                String yi = quantifiers.get(i).getId();
                String yj = quantifiers.get(j).getId();
                inequalities.add(clauseFactory.getInequality(yi, yj));
            }

        }
        clauses.addAll(inequalities);
        Conjunction and = new Conjunction(quantifiers, clauses);
        stack.push(and);
    }

    @Override
    public void visit(OWLObjectExactCardinality owloec) {
//        OWLObjectMinCardinality owlomc = factory.getOWLObjectMinCardinality(0, arg1);
//        factory.getOWLObjectIntersectionOf()
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLObjectMaxCardinality owlomc) {
        // TO DO 
        // Qualified Number restriction ALCQ
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Converts ObjectHasSelf( R ) (or in DL Syntax \exists R.SELF into the FOL formula R(X,X);
     *
     * @param owlohs
     */
    @Override
    public void visit(OWLObjectHasSelf owlohs) {
        String varX = piVar;
//        Quantifier quant = new Quantifier(Quantifier.Type.ALL, varX);
        owlohs.getProperty().accept(this);
        PiClause c = stack.pop();
        Predicate p;
        try {
            p = (Predicate) c;
        } catch (ClassCastException cce) {
            logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                    Predicate.class.getCanonicalName(), c.getClass());
            throw cce;
        }
//        Predicate pred = new Predicate(asList(quant), p.getId(), asList(varX, varX));
        Predicate pred = clauseFactory.getPredicate(p.getId(), asList(varX, varX));
        stack.push(pred);
    }

    @Override
    public void visit(OWLObjectOneOf owlooo) {
        // TO DO
        // Nominals ALCO ontologies
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataSomeValuesFrom o) {
        o.getProperty().accept(this);
        OWLDataRange dr = o.getFiller();

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataAllValuesFrom owldvf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataHasValue owldhv) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataMinCardinality owldmc) {
        // TO DO 
        // Qualified Number restriction ALCQ
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataExactCardinality owldec) {
        // TO DO 
        // Qualified Number restriction ALCQ
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLDataMaxCardinality owldmc) {
        // TO DO 
        // Qualified Number restriction ALCQ
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void visit(OWLObjectProperty owlop) {
        String varX = piVar;
        String varY = linkVar;
        Predicate p = clauseFactory.getPredicate(owlop.toStringID(), asList(varX, varY));
        stack.push(p);
    }

    @Override
    public void visit(OWLObjectInverseOf owloio) {
        owloio.getInverse().accept(this);
        PiClause c = stack.pop();
        Predicate p;
        try {
            p = (Predicate) c;
        } catch (ClassCastException cce) {
            logger.error("Error during execution of Pi conversion. {} was expected but I found {}",
                    Predicate.class.getCanonicalName(), c.getClass());
            throw cce;
        }
        String varX = piVar;
        String varY = linkVar;
        Predicate newP = clauseFactory.getPredicate(p.getId(), asList(varY, varX));
        stack.push(newP);
    }

    @Override
    public void visit(OWLDataProperty owldp) {
        String varX = piVar;
        String varY = linkVar;
        Predicate p = clauseFactory.getPredicate(owldp.toStringID(), asList(varX, varY));
        stack.push(p);
    }

    @Override
    public void visit(OWLAnnotationProperty owlap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void doDefault(Object object) {
        logger.error("Unsupported coversion of class {}", object.getClass());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

    }

    public class PiDataRangeVisitor implements OWLDataRangeVisitor {

        Stack<AbstractPiClause> drClauses = new Stack<>();

        public AbstractPiClause getFOLFormula() {
            return drClauses.pop();
        }

        @Override
        public void visit(OWLDatatype node) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void visit(OWLDataOneOf node) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void visit(OWLDataComplementOf node) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void visit(OWLDataIntersectionOf node) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void visit(OWLDataUnionOf node) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        /**
         * It works only with numeric restrictions
         * @param node
         */
        @Override
        public void visit(OWLDatatypeRestriction node) {
            Conjunction conj = clauseFactory.getConjuntion(new ArrayList<>());
            OWLDatatype dt = node.getDatatype();
            if (dt.isBuiltIn()) {
                
                switch (dt.getBuiltInDatatype()) {
                    case XSD_NON_NEGATIVE_INTEGER:
                    case XSD_UNSIGNED_INT:
                    case XSD_UNSIGNED_LONG:
                    case XSD_UNSIGNED_SHORT:
                        conj.getClauses().add(clauseFactory.getMinInclusive());

                }
                for (OWLFacetRestriction restr : node.getFacetRestrictions()) {
                    OWLFacet facet = restr.getFacet();
                    OWLLiteral facetValue = restr.getFacetValue();
                    switch (facet) {
                        case MAX_EXCLUSIVE:
                    }
                }
            } else {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        }
    }
}
