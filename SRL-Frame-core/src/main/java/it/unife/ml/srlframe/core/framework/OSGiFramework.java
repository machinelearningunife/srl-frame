/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core.framework;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class OSGiFramework {
    
    private OSGiFramework() {}
    
    /**
     * This tells the application plugin whether it should call system exit on quit.
     * 
     * If SRL-Frame is started by the launcher, then the clean shutdown of SRL-Frame is handled by the launcher.
     * However developers will often start SRL-Frame without the launcher and it appears that in that case
     * SRL-Frame does not know when to exit.  This routine tells the SRL-Frame core application plugin if the 
     * System.exit should be called in the SRL-Frame core application handling.
     * 
     */
    public static boolean systemExitHandledByLauncher() {
        String forceExit = System.getProperty("it.unife.ml.srlframe.launcher.launcherHandlesExit");
        return forceExit != null && forceExit.toLowerCase().equals("true");
    }

}