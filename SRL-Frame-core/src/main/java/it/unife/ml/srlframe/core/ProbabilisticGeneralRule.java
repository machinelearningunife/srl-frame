/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.core;

import it.unife.endif.ml.math.ApproxDouble;
import it.unife.ml.srlframe.core.piclauses.AbstractPiClause;
import java.util.List;


/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ProbabilisticGeneralRule {
    
    private List<AbstractPiClause> head;
    
    private List<AbstractPiClause> body;
    
    private ApproxDouble probability;
    
    public ProbabilisticGeneralRule(List<AbstractPiClause> head, List<AbstractPiClause> body, ApproxDouble probability) {
        this.head = head;
        this.body = body;
        this.probability = probability;
    }

    /**
     * @return the head
     */
    public List<AbstractPiClause> getHead() {
        return head;
    }

    /**
     * @return the body
     */
    public List<AbstractPiClause> getBody() {
        return body;
    }

    /**
     * @return the probability
     */
    public ApproxDouble getProbability() {
        return probability;
    }
    
}
