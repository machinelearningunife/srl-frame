package it.unife.ml.srlframe.activator;

import it.unife.ml.srlframe.core.framework.Manager;
import it.unife.ml.srlframe.core.framework.OSGiFramework;
import it.unife.ml.srlframe.core.framework.SRLFrameExecutable;
import it.unife.ml.srlframe.core.log.LogBanner;
import it.unife.ml.srlframe.core.plugin.PluginUtilities;
import static it.unife.ml.srlframe.utils.InfoUtilities.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.List;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.ParsedLine;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SRLFrameApp implements BundleActivator {
    
    private static BundleContext context;
    
    private final Logger logger = LoggerFactory.getLogger(SRLFrameApp.class);
    
    private boolean initialized = false;
    
    @Override
    public void start(BundleContext context) throws Exception {
        // TODO add activation code here
        logger.debug("ciaooooooooooo");
        System.out.println("E quindi??");
        context.addFrameworkListener(event -> {
            if (event.getType() == FrameworkEvent.STARTED) {
                reallyStart(context);
            }
            
        });
    }
    
    public void reallyStart(BundleContext context) {
        try {
            SRLFrameApp.context = context;
            displayPlatform();
            init();
            
            startApplication();
            
        } catch (Exception e) {
            logger.error("Exception caught in SRL-Frame", e);
        }
    }
    
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

    /**
     * Displays some info and check if the plugins contain the plugin.xml file
     */
    private void displayPlatform() {
        Bundle thisBundle = context.getBundle();
        Version v = PluginUtilities.getBundleVersion(thisBundle);
        
        logger.info(LogBanner.start("SRL-Frame"));
        logger.info("SRL-Frame");
        logger.info("Version {}.{}.{}, Build {}", v.getMajor(), v.getMinor(), v.getMicro(), v.getQualifier());
        logger.info("");
        logger.info("");
        logger.info(LogBanner.start("Platform"));
        logger.info("Java: JVM {}  Memory: {}M", System.getProperty("java.runtime.version"), getMaxMemoryInMegaBytes());
        logger.info("Language: {}, Country: {}", getLang(), getCountry());
        logger.info("Framework: {} ({}) ", getFramework(), getFrameworkVersion());
        logger.info("OS: {} ({})", getOsName(), getOsVersion());
        logger.info("Processor: {}\n", getProcessor());
        logger.info(LogBanner.end());
        logger.info(LogBanner.start("Plugins"));
        int pluginCount = 0;
        
        for (Bundle plugin : context.getBundles()) {
            if (isPlugin(plugin)) {
                if (isActive(plugin)) {
                    logger.info("Plugin: {} ({})", getNiceBundleName(plugin), plugin.getVersion());
                    pluginCount++;
                } else {
                    logger.warn("Plugin: {} ({}) was not successfully started.  "
                            + "Please see the Protégé log for more details.", getNiceBundleName(plugin), plugin.getVersion());
                }
            }
        }
        if (pluginCount == 0) {
            logger.info("No plugins installed");
        }
        logger.info(LogBanner.end());
        for (Bundle plugin : context.getBundles()) {
            if (isPlugin(plugin)) {
                pluginSanityCheck(plugin);
            }
        }
    }
    
    private static String getProcessor() {
        return context.getProperty(Constants.FRAMEWORK_PROCESSOR);
    }
    
    private static String getOsVersion() {
        return context.getProperty(Constants.FRAMEWORK_OS_VERSION);
    }
    
    private static String getOsName() {
        return context.getProperty(Constants.FRAMEWORK_OS_NAME);
    }
    
    private static String getFrameworkVersion() {
        return context.getProperty(Constants.FRAMEWORK_VERSION);
    }
    
    private static String getFramework() {
        return context.getProperty(Constants.FRAMEWORK_VENDOR);
    }
    
    public static boolean isActive(Bundle b) {
        return b.getState() == Bundle.ACTIVE;
    }
    
    public static boolean isPlugin(Bundle b) {
        String location = b.getLocation();
        return location != null && location.contains("plugin");
    }
    
    private boolean pluginSanityCheck(Bundle b) {
        boolean passed = true;
        boolean hasPluginXml = (b.getResource("/plugin.xml") != null);
//        if (b.getHeaders().get(BUNDLE_WITHOUT_PLUGIN_XML) == null && !hasPluginXml) {
        if (!hasPluginXml) {
            logger.debug("\t" + getNiceBundleName(b) + " Plugin has no plugin.xml resource");
            passed = false;
        }
        if (hasPluginXml && !isSingleton(b)) {
            logger.warn("\t" + getNiceBundleName(b) + " plugin is not a singleton so its plugin.xml will not be seen by the registry.");
            passed = false;
        }
        return passed;
    }
    
    public static boolean isSingleton(Bundle b) {
        StringBuffer singleton1 = new StringBuffer(Constants.SINGLETON_DIRECTIVE);
        singleton1.append(":=true");
        StringBuffer singleton2 = new StringBuffer(Constants.SINGLETON_DIRECTIVE);
        singleton2.append(":=\"true\"");
        return ((String) b.getHeaders().get(Constants.BUNDLE_SYMBOLICNAME)).contains(singleton1.toString())
                || ((String) b.getHeaders().get(Constants.BUNDLE_SYMBOLICNAME)).contains(singleton2.toString());
    }
    
    public static String getNiceBundleName(Bundle b) {
        String name = (String) b.getHeaders().get(Constants.BUNDLE_NAME);
        if (name == null) {
            name = b.getSymbolicName();
        }
        return name;
    }
    
    protected SRLFrameApp init() throws Exception {
        try {
            PluginUtilities.getInstance().initialise(context);
            loadDefaults();
        } finally {
            initialized = true;
        }
        return this;
    }
    
    private void loadDefaults() {
    }
    
    private void startApplication() throws IOException {
        logger.debug("Starting application");
//        Console console = System.console();
//        String command = console.readLine("srl-frame> ");
        String prompt = "srl-frame> ";
        
        Terminal terminal = TerminalBuilder.terminal();
        LineReader lineReader = LineReaderBuilder.builder()
                .terminal(terminal)
                .build();
        while (true) {
            String line = lineReader.readLine(prompt);
            ParsedLine pl = lineReader.getParser().parse(line, 0);
            List<String> args = new ArrayList<>(pl.words());

            /*
             * Used for debug
             */
//            String line = "owlkraider http://dbpedia.org/resource/Angela_Merkel";
//            List<String> args = new ArrayList<>(asList(line.split("\\s+")));
            /* -- */
            String command = args.remove(0);
            if (command.equalsIgnoreCase("install")) {
                if (args.size() < 2) {
                    // Error
//                    terminal.writer().println("Error no plugin to install was given");
                }
                for (int i = 1; i < args.size(); i++) {
                    URI uri = null;
                    try {
                        uri = new URI(args.get(i));
                    } catch (URISyntaxException e) {
//                        terminal.writer().println("Malformed URI: " + uri);
                        continue;
                    }
                    // TO DO: download of the jar

                    File jarBundle = new File(uri);
                }
            } else if (command.equalsIgnoreCase("exit")) {
                break;
            } else if (command.trim().isEmpty()) {
                continue;
            } else {
                try {
                    Manager manager = new Manager(context);
                    SRLFrameExecutable executable = manager.getExecutable(command, args);
                    executable.execute();
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        handleQuit();
    }
    
    public void handleQuit() {
        try {
            boolean forceExit = !OSGiFramework.systemExitHandledByLauncher(); // this call fails after context.getBundle(0).stop()
            context.getBundle(0).stop();
            
            if (forceExit) {
                Thread.sleep(1000);
                System.exit(0);
            }
        } catch (Throwable t) {
            logger.error("An error occurred when shutting down SRL-Frame.", t);
        }
    }
    
}
