package it.unife.ml.srlframe.common;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;


public class ActivatorNoLog implements BundleActivator {

    private ServiceListener listener;

    @Override
    public void start(BundleContext context) throws Exception {
        System.out.println("Starting " + this.getClass().getPackage() + " bundle");
//		context.registerService(
//	        javax.xml.parsers.SAXParserFactory.class.getName(),
//	        javax.xml.parsers.SAXParserFactory.newInstance(), null);
//        System.out.println("IsDebugEnabled: " + logger.isDebugEnabled());
//        if (logger.isDebugEnabled()) {
//            startDebug(context);
//        }
    }

    public void startDebug(BundleContext context) {
        listener = new DebugServicesListener();
        context.addServiceListener(listener);
    }

    private class DebugServicesListener implements ServiceListener {

        @Override
        public void serviceChanged(ServiceEvent event) {
            switch (event.getType()) {
                case ServiceEvent.REGISTERED:
                    System.out.println(event.getServiceReference().getBundle().getSymbolicName() + " registering service " + displayClass(event.getServiceReference()));
                    break;
                case ServiceEvent.UNREGISTERING:
                    System.out.println(event.getServiceReference().getBundle().getSymbolicName() + " unregistering service " + displayClass(event.getServiceReference()));
                    break;
                default:
                    System.out.println("Modifying service " + event.getServiceReference());
                    break;
            }
        }

        private String displayClass(ServiceReference ref) {
            Object classes = ref.getProperty(Constants.OBJECTCLASS);
            if (classes instanceof String[]) {
                String classArray[] = (String[]) classes;
                return classArray[0];
            }
            return classes.toString();
        }
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if (listener != null) {
            context.removeServiceListener(listener);
            listener = null;
        }
    }

}