import com.modeliosoft.modelio.javadesigner.annotations.objid;

@objid ("e20157b2-e413-425c-b888-2aac14c7b07a")
public interface PluginFactory<T extends SRLFramePlugin> {
    @objid ("23cf0c6e-ae3b-47ba-9e4f-4304af02b834")
    T createInstance();

}
