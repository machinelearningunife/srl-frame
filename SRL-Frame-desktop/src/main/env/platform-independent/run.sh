#!/usr/bin/env bash

java -Dlog4j.configurationFile=$(pwd)/conf/log4j2.xml \
     -Dfile.encoding=UTF-8 \
     -classpath "bin/*:dependencies/*" \
     it.unife.ml.srlframe.launcher.Launcher conf/config.xml