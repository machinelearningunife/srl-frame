/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import it.unife.ml.srlframe.launcher.Launcher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class LauncherIT {

    public LauncherIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    private final String CONFIG_FILE_PATH = "src/test/resources/conf/config.xml";

    /**
     * Test of main method, of class Launcher.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = {CONFIG_FILE_PATH};
        try {
            Launcher.main(args);
        } catch (Exception e) {
              fail("Failed to launch framework. Error: " + e.getMessage() + " " + e.getCause().getLocalizedMessage());
        }
    }

}
