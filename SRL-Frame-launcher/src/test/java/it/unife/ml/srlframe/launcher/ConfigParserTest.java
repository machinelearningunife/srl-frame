/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import it.unife.ml.srlframe.launcher.BundleInfo;
import it.unife.ml.srlframe.launcher.ConfigParser;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ConfigParserTest {

    @Rule
    public ErrorCollector errorCollector = new ErrorCollector();

    public ConfigParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    private final String CONFIG_FILE_PATH = "src/test/resources/conf/config.xml";

     /**
     * Test of parse method, of class ConfigParser.
     */
    @Test
    public void testParse() {
        System.out.println("parse");
        File configFile = new File(CONFIG_FILE_PATH);
        ConfigParser instance = new ConfigParser();
        try {
            instance.parse(configFile);
        } catch (Exception e) {
            fail("Failed to parse file. Error: " + e.getMessage() + " " + e.getCause().getLocalizedMessage());
        }
        Map<String, String> systemProperties = instance.getSystemProperties();
        Map<String, String> frameworkProperties = instance.getFrameworkProperties();
        List<BundleInfo> bundleInfos = instance.getBundleInfos();
        assertTrue(systemProperties.containsKey("file.encoding"));
        assertTrue(frameworkProperties.containsKey("org.osgi.framework.storage.clean"));
        List<BundleInfo> expectedBundleInfos = new ArrayList<>();
        expectedBundleInfos.add(new BundleInfo(Arrays.asList(
                new File[] {new File("bundles"), new File("../SRL-Frame-common/target")}), 
                "srlframe-common.jar"));
        //expectedBundleInfos.add(new BundleInfo(Arrays.asList(new File("bundles")), "srlframe-core.jar"));
        assertEquals(expectedBundleInfos, bundleInfos);
    }

    /**
     * Test of reset method, of class ConfigParser.
     */
    @Test
    public void testReset() {
        System.out.println("reset");
        ConfigParser instance = new ConfigParser();
        instance.reset();
        assertTrue(instance.getFrameworkProperties().isEmpty());
        assertTrue(instance.getFrameworkProperties().isEmpty());
        assertTrue(instance.getBundleInfos().isEmpty());
        File configFile = new File(CONFIG_FILE_PATH);
        try {
            instance.parse(configFile);
        } catch (Exception e) {
            fail("Failed to parse file. Error: " + e.getMessage());
        }
        instance.reset();
        assertTrue(instance.getFrameworkProperties().isEmpty());
        assertTrue(instance.getFrameworkProperties().isEmpty());
        assertTrue(instance.getBundleInfos().isEmpty());
    }

    /**
     * Test of getBundleInfos method, of class ConfigParser.
     */
//    @Test
    public void testGetBundleInfos() {
        System.out.println("getBundleInfos");
        ConfigParser instance = new ConfigParser();
        List<BundleInfo> expResult = null;
        List<BundleInfo> result = instance.getBundleInfos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
