/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import org.apache.felix.framework.Logger;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class OSGiSLF4JLogger extends Logger {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(OSGiSLF4JLogger.class);

    @Override
    protected void doLog(Bundle bundle, ServiceReference sr, int level, String msg, Throwable throwable) {
        if (level == LOG_ERROR) {
            logger.error(msg, throwable);
        } else if (level == LOG_WARNING) {
            logger.warn(msg, throwable);
        } else if (level == LOG_INFO) {
            logger.info(msg, throwable);
        } else if (level == LOG_DEBUG) {
            logger.debug(msg, throwable);
        } 
    }
}

