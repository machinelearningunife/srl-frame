/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import it.unife.ml.srlframe.launcher.exceptions.MissingBundleInfoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.resolver.DefaultEntityResolver;
import org.apache.commons.configuration2.tree.ImmutableNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class ConfigParser {

    public static final String PROPERTY = "property";
    public static final String NAME = "name";
    public static final String VALUE = "value";
    public static final String FRAMEWORK_CONFIG = "frameworkProperties";
    public static final String SYSTEM_CONFIG = "systemProperties";
    public static final String BUNDLES_CONFIG = "bundles";

    private final Logger logger = LoggerFactory.getLogger(ConfigParser.class.getCanonicalName());

    Configurations configs = new Configurations();

    private Map<String, String> frameworkProperties;
    private Map<String, String> systemProperties;
    private List<BundleInfo> bundleInfos = new ArrayList<>();

    public Map<String, String> getFrameworkProperties() {
        return frameworkProperties;
    }

    public Map<String, String> getSystemProperties() {
        return systemProperties;
    }

    public List<BundleInfo> getBundleInfos() {
        return bundleInfos;
    }

    public void parse(File configFile) throws ConfigurationException, FileNotFoundException, MissingBundleInfoException {
        if (!configFile.exists()) {
            String message = "File not found: " + configFile.getAbsolutePath();
            throw new FileNotFoundException(message);
        }

        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<XMLConfiguration> builder
                = new FileBasedConfigurationBuilder<XMLConfiguration>(XMLConfiguration.class)
                        .configure(params.xml()
                                .setFileName(configFile.getAbsolutePath())
                                .setSchemaValidation(true));

// This will throw a ConfigurationException if the XML document does not
// conform to its Schema.
        XMLConfiguration config = builder.getConfiguration();

//        XMLConfiguration config = configs.xml(configFile);
//        config.setSchemaValidation(true);
//        config.setEntityResolver(new DefaultEntityResolver());
//        config.validate();
        // access configuration properties
        systemProperties = readProperties(config.configurationAt(SYSTEM_CONFIG));
        frameworkProperties = readProperties(config.configurationAt(FRAMEWORK_CONFIG));
        bundleInfos = readBundleInfos(config.configurationAt(BUNDLES_CONFIG));
    }

    public void reset() {
        frameworkProperties = new TreeMap<>();
        systemProperties = new TreeMap<>();
        bundleInfos.clear();
    }

    private Map<String, String> readProperties(HierarchicalConfiguration<ImmutableNode> config) {
        List<String> propertyNames = config.getList(String.class, "property[@" + NAME + "]");
        List<String> propertyValues = config.getList(String.class, "property[@" + VALUE + "]");
        Map<String, String> propertyMap = new TreeMap<>();
        for (int i = 0; i < propertyNames.size(); i++) {
            String propName = propertyNames.get(i);
            String propValue = propertyValues.get(i);
            propertyMap.put(propName, propValue);
        }
        return propertyMap;
    }

    private List<BundleInfo> readBundleInfos(HierarchicalConfiguration<ImmutableNode> config) throws MissingBundleInfoException {
        List<BundleInfo> bundleInfos = new ArrayList<>();
        List<HierarchicalConfiguration<ImmutableNode>> bundleNodes = config.configurationsAt("bundle");
        for (HierarchicalConfiguration<ImmutableNode> bundleNode : bundleNodes) {
            List<String> pathStrings = bundleNode.getList(String.class, "path");
            List<File> pathFiles = new ArrayList<>(pathStrings.size());
            String jarName = bundleNode.getString("[@" + NAME + "]");
            if (jarName == null) { // a path containing multiple jars
                
            } else {

                for (String path : pathStrings) {
                    pathFiles.add(new File(path));
                }
//            if (jarName.isEmpty()) {
//                String msg = "";
//                throw new MissingBundleInfoException(msg);
//            }
//            if (pathFiles.isEmpty()) {
//                String msg = "";
//                throw new MissingBundleInfoException(msg);
//            }
                bundleInfos.add(new BundleInfo(pathFiles, jarName));
            }

        }
        return bundleInfos;
    }

}
