/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher.exceptions;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MissingBundleInfoException extends Exception {

    /**
     * Creates a new instance of <code>MissingBundleInfoException</code> without detail message.
     */
    public MissingBundleInfoException() {
    }

    /**
     * Constructs an instance of <code>MissingBundleInfoException</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public MissingBundleInfoException(String msg) {
        super(msg);
    }
}
