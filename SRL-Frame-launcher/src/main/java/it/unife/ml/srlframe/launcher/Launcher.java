/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import it.unife.ml.srlframe.launcher.exceptions.MissingBundleInfoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.felix.main.AutoProcessor;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;
import org.osgi.framework.startlevel.BundleStartLevel;
import org.osgi.framework.startlevel.FrameworkStartLevel;
import org.osgi.framework.wiring.BundleRevision;
import org.osgi.service.startlevel.StartLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Launcher instantiates a new OSGi Framework and starts all the bundles
 * reported in a configuration file.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Launcher {

    private final Logger logger = LoggerFactory.getLogger(Launcher.class.getCanonicalName());
    public static final String FELIX_LOG_LOGGER = "felix.log.logger";

    public static final String FELIX_SHUTDOWN_HOOK = "felix.shutdown.hook";

    private final Map<String, String> frameworkProperties = new TreeMap<>();

    // list of the bundles that will be installed and started
    private List<File> bundleJars = new ArrayList<>();

    private Framework framework;

    public Launcher(File configFile) throws ConfigurationException, FileNotFoundException, MissingBundleInfoException {
        // Read the properties from the configuration file and put them in frameworkProperties
        ConfigParser parser = new ConfigParser();
        parser.parse(configFile);
        // set system properties read from the configuration file
        setSystemProperties(parser);
        // add framework properties
        frameworkProperties.putAll(parser.getFrameworkProperties());
        frameworkProperties.put(FELIX_SHUTDOWN_HOOK, "true");
        setLogger(frameworkProperties);
        // get the bundles
        List<BundleInfo> bundleInfos = parser.getBundleInfos();
        for (BundleInfo bundleInfo : bundleInfos) {
            List<File> validPaths = bundleInfo.getValidPaths();
            if (validPaths.size() > 0) {
                bundleJars.add(bundleInfo.getValidPaths().get(0));
            } else {
                logger.warn("Bundle {} not found", bundleInfo.getJarFilePath());
//                System.out.println("Bundle " + bundleInfo.getJarFilePath() + " not found");
            }
        }
        frameworkProperties.put(Constants.FRAMEWORK_BEGINNING_STARTLEVEL, Integer.toString(bundleJars.size()));
    }

    /**
     * This method gets from the parser all the system properties defined in the
     * configuration file and set them.
     *
     * @param p
     */
    private void setSystemProperties(ConfigParser p) {
        Map<String, String> systemProperties = p.getSystemProperties();
        System.setProperty("it.unife.ml.srlframe.launcher.launcherHandlesExit", "true");
        for (Map.Entry<String, String> entry : systemProperties.entrySet()) {
            System.setProperty(entry.getKey(), entry.getValue());
        }
    }

    private static FrameworkFactory getFrameworkFactory() throws Exception {
        java.net.URL url = Launcher.class.getClassLoader().getResource(
                "META-INF/services/org.osgi.framework.launch.FrameworkFactory");
        if (url != null) {
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            try {
                for (String s = br.readLine(); s != null; s = br.readLine()) {
                    s = s.trim();
                    // Try to load first non-empty, non-commented line.
                    if ((s.length() > 0) && (s.charAt(0) != '#')) {
                        return (FrameworkFactory) Class.forName(s).newInstance();
                    }
                }
            } finally {
                if (br != null) {
                    br.close();
                }
            }
        }

        throw new Exception("Could not find framework factory.");
    }

    /**
     * Sets the default felix logger so that logging output is redirected to
     * SLF4J instead of stderr and stdout
     *
     * @param configurationMap The configuration map. Note that the framework
     * factory newFramework method expects a map that maps Strings to Strings.
     * However, the documentation for the Felix configuration properties
     * specifies that the config value of the felix.log.logger property must be
     * an instance of Logger. This method therefore makes an unchecked call to
     * Map.put(), which works.
     */
    @SuppressWarnings("unchecked")
    private static void setLogger(Map configurationMap) {
        OSGiSLF4JLogger logger = new OSGiSLF4JLogger();
        configurationMap.put(FELIX_LOG_LOGGER, logger);
    }

    private void printBanner() {
        logger.info("********************************************************************************");
        logger.info("**                                 SRL-Frame                                  **");
        logger.info("********************************************************************************");
        logger.info("");
    }

    private void start() throws
            IOException, ClassNotFoundException, InstantiationException, IllegalAccessException,
            BundleException, InterruptedException, Exception {
        printBanner();
        logger.info("----------------- Initialising and Starting the OSGi Framework -----------------");
        FrameworkFactory frameworkFactory = getFrameworkFactory();
        logger.info("FrameworkFactory Class: {}", frameworkFactory.getClass().toString());
        logger.info("");

        // If enabled, register a shutdown hook to make sure the framework is
        // cleanly shutdown when the VM exits.
        String enableHook = frameworkProperties.get(FELIX_SHUTDOWN_HOOK);
        if ((enableHook == null) || !enableHook.equalsIgnoreCase("false")) {
            addShutdownHook();
        }

        framework = frameworkFactory.newFramework(frameworkProperties);
        framework.init();
        logger.info("The OSGi framework has been initialised");
//        System.out.println("The OSGi framework has been initialised");

        AutoProcessor.process(frameworkProperties, framework.getBundleContext());
        BundleContext context = framework.getBundleContext();
        List<Bundle> bundles = new ArrayList<>();
        // Retrieve the Start Level service, since it will be needed
        // to set the start level of the installed bundles.
//        StartLevel sl = (StartLevel) context.getService(
//                context.getServiceReference(StartLevel.class.getName()));

        // Get start level for auto-deploy bundles.
//        int startLevel = sl.getInitialBundleStartLevel();
        int startLevel = 1;

        for (File bundleJar : bundleJars) {
            bundles.addAll(installBundles(context, bundleJar, startLevel++));
        }
        startBundles(bundles);
        try {
            framework.start();
//            startBundles(bundles);
            FrameworkStartLevel level = framework.adapt(FrameworkStartLevel.class);
            logger.info("{}", level.getStartLevel());
            logger.info("The OSGi framework has been started");
            logger.info("");
        } catch (BundleException e) {
            logger.error("An error occurred when starting the OSGi framework: {}", e.getMessage(), e);
            throw e;
        }

//        for (int i = 0; i < 20; i++) {
//            logger.info("{}", i);
//            Thread.sleep(1000);
//        }
        // Wait for framework to stop to exit the VM.
        framework.waitForStop(0);
        System.exit(0);
    }

    private List<Bundle> installBundles(BundleContext context, File bundleFile, int startLevel) throws BundleException {
        List<Bundle> core = new ArrayList<>();
        try {
            String bundleURI = bundleFile.getAbsoluteFile().toURI().toString();
            logger.debug("Installing bundle.  StartLevel: {}; Bundle: {}", startLevel, bundleFile.getAbsolutePath());
            Bundle bundle = context.installBundle(bundleURI);

            BundleStartLevel bundleStartLevel = bundle.adapt(BundleStartLevel.class);
            bundleStartLevel.setStartLevel(startLevel);
            // the cast to BundleStartLevel is not needed in Java 6 but it is in Java 7
//            ((BundleStartLevel) newBundle.adapt(BundleStartLevel.class)).setStartLevel(startLevel);
            core.add(bundle);
        } catch (Throwable t) {
            logger.warn("Bundle {} failed to install: {}", bundleFile, t);
        }
        return core;
    }

    private void startBundles(List<Bundle> bundles) throws BundleException {
        logger.info("------------------------------- Starting Bundles -------------------------------");
        for (Bundle b : bundles) {
            try {
                if (!isFragmentBundle(b)) {
                    b.start(2);
                    logger.info("Starting bundle {}", b.getSymbolicName());
                } else {
                    logger.info("Not starting bundle {} explicitly because it is a fragment bundle.", b.getSymbolicName());
                }
            } catch (Throwable t) {
                logger.error("Core Bundle {} failed to start: {}", b.getBundleId(), t);
            }
        }
        logger.debug("-------------------------------------------------------------------------------");
    }

    private static boolean isFragmentBundle(Bundle b) {
        return (b.adapt(BundleRevision.class).getTypes() & BundleRevision.TYPE_FRAGMENT) != 0;
    }

    private void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {

            public void run() {
                try {
                    logger.info("----------------------- Shutting down SRL-Frame -----------------------");
                    if (framework != null) {
                        framework.stop();
                        framework.waitForStop(0);
                    }
                } catch (Throwable t) {
                    logger.error("Error shutting down OSGi session: {}", t.getMessage(), t);
                }
            }
        }
        );

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        System.out.println("Mi sono avviato");
        System.out.println(System.getProperty("user.dir"));
        File configFile = new File(args[0]);
        Launcher launcher = new Launcher(configFile);
        launcher.start();
    }

}
