/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.srlframe.launcher;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all the possible paths of a bundle. The first valid path containing the
 * bundle will be used.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
//class BundleInfo implements Comparable<BundleInfo>{
class BundleInfo {

    public static final String USER_HOME = "user.home";

    public static final String USER_HOME_VAR = "${" + USER_HOME + "}/";

    private List<File> paths = new ArrayList<>();

    private String jarFile;

    public BundleInfo(List<File> paths, String jarFile) {
        this.paths = paths;
        this.jarFile = jarFile;
    }

    /**
     * @return the paths The paths where the bundle could be found
     */
    public List<File> getPaths() {
        return paths;
    }

    /**
     * @return the jarFile the filename of the bundle (<filename>.jar).
     */
    public String getJarFilePath() {
        return jarFile;
    }

    public void addPath(String dir) {
        if (dir.startsWith(USER_HOME_VAR)) {
            String homeDirectory = System.getProperty(USER_HOME);
            dir = dir.substring(USER_HOME_VAR.length());
            paths.add(new File(homeDirectory, dir));
        } else {
            paths.add(new File(dir));
        }
    }

    /**
     * This method return the paths that exist and that contain the jar file indicated in
     * {@link BundleInfo#jarFile}
     *
     * @return
     */
    public List<File> getValidPaths() {
        List<File> validPaths = new ArrayList<>();
        for (File dir : paths) {
            if (!dir.exists() || !dir.isDirectory()) {
                continue;
            } else {
                File[] jarFiles = dir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
//                        return name.toLowerCase().endsWith(".jar");
                        return name.equalsIgnoreCase(jarFile);
                    }
                });
                switch (jarFiles.length) {
                    case 0:
                        break;
                    case 1:
                        validPaths.add(new File(dir, jarFile));
                        break;
                    default:
                        // Impossible condition
                        String msg = "Duplicated file " + jarFile + " in " + dir.getAbsolutePath();
                        throw new Error(msg);
                }
            }
        }
        return validPaths;
    }

//    @Override
//    public int compareTo(BundleInfo o) {
//        int compare = this.jarFile.compareTo(o.jarFile);
//        if (compare != 0) {
//            return compare;
//        } else {
//            compare = this.paths.size() - o.paths.size();
//            if (compare != 0) {
//                return compare;
//            } else {
//                for (int i = 0; i < this.paths.size(); i++) {
//                    compare = this.paths.get(i).getAbsolutePath().compareTo(o.paths.get(i).getAbsolutePath());
//                    if (compare != 0) {
//                        return compare;
//                    }
//                }
//                return compare;
//            }
//        }
//        
//    }
    @Override
    public boolean equals(Object obj) {
        BundleInfo o = (BundleInfo) obj;
        int compare = this.jarFile.compareTo(o.jarFile);
        if (compare != 0) {
            return false;
        } else {
            compare = this.paths.size() - o.paths.size();
            if (compare != 0) {
                return false;
            } else {
                for (int i = 0; i < this.paths.size(); i++) {
                    compare = this.paths.get(i).getAbsolutePath().compareTo(o.paths.get(i).getAbsolutePath());
                    if (compare != 0) {
                        return false;
                    }
                }
                return true;
            }
        }

    }

}
