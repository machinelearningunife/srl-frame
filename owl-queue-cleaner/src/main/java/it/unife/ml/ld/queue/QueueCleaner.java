/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld.queue;

import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import it.unife.ml.ld.Axiom;
import it.unife.ml.kraider.api.Triple;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import javax.swing.event.ChangeEvent;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to clean the queue retrieved by KRaider. It converts the
 * extracted triples into OWLAxioms. It gets an input queue (queue of triples)
 * and an output queue (output of OWLAxiom). It starts a thread that converts
 * each element of the queue of triples.
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class QueueCleaner {

    private static final Logger logger = LoggerFactory.getLogger(QueueCleaner.class);

    private BlockingQueue<Triple> tripleQueue;

    private BlockingQueue<Axiom> axiomQueue = new LinkedBlockingQueue<>();

    private QueueCleanerConfiguration configuration;

    private long tripleCount = 0;

    private List<QueueListener> listeners = new ArrayList<>();

    /**
     * This map contains a mapping between blank nodes and set of triples. Each
     * blank node is mapped to the set of triples involving it (both as subject
     * and as object).
     *
     * TO DO: we cannot keep forever these triples, after a while they should be
     * converted or discarded.
     */
    private Map<String, Set<Triple>> blankNodeTriples = new HashMap<>();

    /*
    * This map contains a mapping between blank nodes and the last time that a
    * triple involving it was added.
     */
    private Map<String, Long> blankNodeLastTripleAdded = new HashMap<>();

    /**
     * Triples that was not possible to convert yet, for some reasons
     */
    private Set<Triple> suspendedTriples = new TreeSet<>();

    private OWLDataFactory df;

    private boolean stop = false;

    public QueueCleaner(BlockingQueue<Triple> inputTripleQueue, QueueCleanerConfiguration configuration) {
        this.tripleQueue = inputTripleQueue;
        this.configuration = configuration;
        this.df = OWLManager.getOWLDataFactory();
//        this.axiomQueue = outputAxiomQueue;
    }

    public BlockingQueue<Axiom> startConversion() {
        ConversionThread converter = new ConversionThread();
        Thread t = new Thread(converter);
        t.start();
        return axiomQueue;
    }

    public void addListener(QueueListener listener) {
        listeners.add(listener);
    }

    private class ConversionThread implements Runnable {

        Triple2OWLAxiomConverter converter = new Triple2OWLAxiomConverter(configuration);

        @Override
        public void run() {

            while (!stopCriteria()) {
                try {
                    Triple triple = tripleQueue.poll(configuration.getTriplePollTimeout(), TimeUnit.SECONDS);
                    if (triple == null) { // timeout
                        stop = true;
                    } else {
                        Set<Axiom> axioms = convertTriple(triple);
                        // increment count of triples
                        tripleCount++;
                        if (axioms != null && !axioms.isEmpty()) {
                            axiomQueue.addAll(axioms);
                            listeners.forEach((listener) -> {
                                listener.stateChanged(new ChangeEvent(axioms));
                            });
                        } else {

                        }

                    }

                } catch (InterruptedException ie) {
                    logger.error(ie.getLocalizedMessage());
//                    throw new RuntimeException(ie.getLocalizedMessage());
                }
            }

        }

        private Set<Axiom> convertTriple(Triple triple) {
            Set<Axiom> allAxioms = new TreeSet<>();
            if (triple.getSubject().isAnon() || triple.getObject().isAnon()) {
                // TO DO: handle blank nodes!!!

                throw new UnsupportedOperationException("Not supported yet.");

            } else {

                Set<OWLAxiom> owlAxioms = new HashSet<>();
                owlAxioms.addAll(safe(converter.convertTriple(triple)));
                if (!owlAxioms.isEmpty()) {
                    owlAxioms.forEach((owlAxiom) -> {
                        allAxioms.add(new Axiom(owlAxiom, triple.getOrigin()));
                    });

                } else {
                    // check if some suspended triples can be converted
                    Set<Triple> suspendedTriplesCopy = new TreeSet<>(suspendedTriples);
                    suspendedTriplesCopy.forEach((t) -> {
                        Set<OWLAxiom> suspendedAxioms = new HashSet<>();
                        owlAxioms.addAll(safe(converter.convertTriple(t)));
                        if (!safe(suspendedAxioms).isEmpty()) {
                            suspendedTriples.remove(t);
                            safe(suspendedAxioms).forEach((owlAxiom) -> {
                                allAxioms.add(new Axiom(owlAxiom, triple.getOrigin()));
                            });
                        }
                    });
                    // add to suspended triples
                    suspendedTriples.add(triple);
                }
                return allAxioms;

            }

        }

    }

    private synchronized boolean stopCriteria() {
        return stop;
    }

    public synchronized void stop() {
        stop = true;
    }

}
