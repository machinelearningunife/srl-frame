/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld.queue;

import it.unife.ml.kraider.exception.ConfigurationException;
import it.unife.ml.ld.queue.QueueCleanerConfiguration.TypeBetweenClassesPolicy;
import static it.unife.ml.ld.queue.QueueCleanerConfiguration.TypeBetweenClassesPolicy.CONVERT_INTO_SUBCLASS;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class QueueCleanerConfigurationBuilder {

    protected TypeBetweenClassesPolicy typeBetweenClassesPolicy = CONVERT_INTO_SUBCLASS;

    
    /**
     * 
     */
    protected int blankNodeBufferSize = 0;

    /**
     * Timeout of triple polling in seconds. Default value: 30 seconds
     *
     */
    protected long triplePollTimeout = 30;

    /**
     * General timeout (default: Long.MAX_VALUE).
     */
    protected long timeout = Long.MAX_VALUE;

    public QueueCleanerConfiguration buildConfiguration()  throws ConfigurationException {
        QueueCleanerConfiguration configuration = new QueueCleanerConfiguration(
                typeBetweenClassesPolicy,
                blankNodeBufferSize,
                triplePollTimeout,
                timeout);

        // check if validation constraints of configuration are met
        validateConfiguration(configuration);

        return configuration;
    }
    
    public QueueCleanerConfigurationBuilder typeBetweenClassesPolicy(TypeBetweenClassesPolicy typeBetweenClassesPolicy) {
        this.typeBetweenClassesPolicy = typeBetweenClassesPolicy;
        return this;
    }

    public QueueCleanerConfigurationBuilder blankNodeBufferSize(int blankNodeBufferSize) {
        this.blankNodeBufferSize = blankNodeBufferSize;
        return this;
    }
            
    public QueueCleanerConfigurationBuilder triplePollTimeout(long triplePollTimeout) {
        this.triplePollTimeout = triplePollTimeout;
        return this;
    }
    
    public QueueCleanerConfigurationBuilder timeout(long timeout) {
        this.timeout = timeout;
        return this;
    }
            
            
    private void validateConfiguration(QueueCleanerConfiguration configuration) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<QueueCleanerConfiguration>> violations = validator.validate(configuration);

        // throws first violation
        if (violations.iterator().hasNext()) {
            ConstraintViolation<QueueCleanerConfiguration> violation = violations.iterator().next();
            throw new ConfigurationException(violation.getPropertyPath() + " " + violation.getMessage());
        }
    }

}
