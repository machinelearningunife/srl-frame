/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld;

import it.unife.ml.kraider.api.Origin;
import java.util.Comparator;
import org.semanticweb.owlapi.model.OWLAxiom;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Axiom implements Comparable<Axiom> {
    
    protected Origin origin;
    
    protected OWLAxiom axiom;
    
    public Axiom(OWLAxiom axiom, Origin origin) {
        this.axiom = axiom;
        this.origin = origin;
    }

    /**
     * @return the origin
     */
    public Origin getOrigin() {
        return origin;
    }

    /**
     * @return the axiom
     */
    public OWLAxiom getAxiom() {
        return axiom;
    }

    @Override
    public int compareTo(Axiom o) {
        return Comparator.comparing(Axiom::getAxiom).thenComparing(Axiom::getOrigin).compare(this, o);
    }
    
}
