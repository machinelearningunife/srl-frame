/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld.queue;

import static it.unife.ml.probowlapi.utilities.GeneralUtils.safe;
import it.unife.ml.kraider.api.Triple;
import it.unife.ml.kraider.api.TripleNode;
import it.unife.ml.kraider.api.TripleNodeVisitor;
import static it.unife.ml.ld.queue.QueueCleanerConfiguration.TypeBetweenClassesPolicy.CONVERT_INTO_SUBCLASS;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.PrefixManager;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Triple2OWLAxiomConverter {

    private static final Logger logger = LoggerFactory.getLogger(QueueCleaner.class);

    private OWLDataFactory df = OWLManager.getOWLDataFactory();

    private PrefixManager pm = new DefaultPrefixManager();

    private final Set<String> classes = new TreeSet<>();

    private final Set<String> datatypes = new TreeSet<>();

    private final Set<String> objectProperties = new TreeSet<>();

    private final Set<String> dataProperties = new TreeSet<>();

    private Set<String> annotationProperties = new TreeSet<>();

    private Set<String> namedIndividuals = new TreeSet<>();

    private QueueCleanerConfiguration configuration;

    /**
     * Triples that was not possible to convert yet, for some reasons
     */
    private Set<Triple> suspendedTriples = new TreeSet<>();

    public Triple2OWLAxiomConverter(QueueCleanerConfiguration conf) {
        this.configuration = conf;
    }

    public Set<OWLAxiom> convertSuspendedTriples() {

        Set<Triple> suspendedTriplesCopy = new TreeSet<>(suspendedTriples);
        Set<OWLAxiom> allAxioms = new TreeSet<>();

        suspendedTriplesCopy.forEach((t) -> {
            Set<OWLAxiom> axioms = convertTriple(t);
            if (!safe(axioms).isEmpty()) {
                allAxioms.addAll(axioms);
                suspendedTriples.remove(t);
            }
        });

        return allAxioms;

    }

    public Set<OWLAxiom> convertTriple(Triple triple) {

        try {
            TripleNode subject = triple.getSubject();
            TripleNode predicate = triple.getPredicate();
            TripleNode object = triple.getObject();

            if (subject.isAnon() || object.isAnon()) {
                suspendedTriples.add(triple);
                return null;
            }

            TripleNodeVisitor rv = new TripleNodeConverterVisitor();

            if (object.isLiteral()) {

                OWLLiteral literal = (OWLLiteral) object.visitWith(rv);

                try {
                    OWLRDFVocabulary builtinAnnotation = OWLRDFVocabulary.valueOf(predicate.toString());
                    OWLAnnotation annotation = df.getOWLAnnotation(
                            df.getOWLAnnotationProperty(IRI.create(predicate.toString())),
                            literal);
                    switch (builtinAnnotation) {
                        case OWL_BACKWARD_COMPATIBLE_WITH:
                        case OWL_DEPRECATED:
                        case OWL_INCOMPATIBLE_WITH:
                        case OWL_PRIOR_VERSION:
                        case OWL_VERSION_INFO:
                        case RDFS_COMMENT:
                        case RDFS_IS_DEFINED_BY:
                        case RDFS_LABEL:
                        case RDFS_SEE_ALSO:
                            OWLAxiom axiom = df.getOWLAnnotationAssertionAxiom(IRI.create(subject.toString()), annotation);
                            return Collections.singleton(axiom);
                        default:
                            String msg = "Unknown annotation usage: " + builtinAnnotation;
                            logger.error(msg);
                            throw new UnsupportedOperationException(msg);
                    }

                } catch (IllegalArgumentException iae) {
                    // data property case
                    // check if the subject is a class (this is not allowed)
                    if (isClass(subject)) {
                        logger.error("Invalid subject in datapropertyaxiom. Subject: {} is a class. ", subject.toString());
                        throw new IllegalArgumentException("Invalid subject in datapropertyaxiom.");
                    }

                    OWLNamedIndividual individual = df.getOWLNamedIndividual(IRI.create(subject.toString()));
                    namedIndividuals.add(subject.toString());

                    OWLDataProperty property = df.getOWLDataProperty(IRI.create(predicate.toString()));
                    dataProperties.add(predicate.toString());
                    OWLAxiom axiom = df.getOWLDataPropertyAssertionAxiom(property, individual, literal);
                    return Collections.singleton(axiom);
                }

            } else if (object.isURIResource()) {
                // if p is different than:
                //  - rdf:type (this can cause problems)
                //  - 
                // then p is an owlobjectproperty and o an instance                     
                try {
                    OWLRDFVocabulary builtinProperty = OWLRDFVocabulary.valueOf(predicate.toString());
                    switch (builtinProperty) {
                        case RDF_TYPE: {
                            Set<OWLAxiom> axioms = convertRDFTypeTriple(triple);
                            return axioms;
                        }
                        case OWL_EQUIVALENT_CLASS: {
                            // it means that subject and object are two classes
                            classes.add(subject.toString());
                            classes.add(object.toString());
                            OWLClass clsA = df.getOWLClass(IRI.create(subject.toString()));
                            OWLClass clsB = df.getOWLClass(IRI.create(object.toString()));
                            OWLAxiom axiom = df.getOWLEquivalentClassesAxiom(clsA, clsB);
                            return Collections.singleton(axiom);
                        }
                        case OWL_DISJOINT_WITH: {
                            // it means that subject and object are two classes
                            classes.add(subject.toString());
                            classes.add(object.toString());
                            OWLClass clsA = df.getOWLClass(IRI.create(subject.toString()));
                            OWLClass clsB = df.getOWLClass(IRI.create(object.toString()));
                            OWLAxiom axiom = df.getOWLDisjointClassesAxiom(clsA, clsB);
                            return Collections.singleton(axiom);
                        }
                        case RDFS_SUBCLASS_OF: {
                            // it means that subject and object are two classes
                            classes.add(subject.toString());
                            classes.add(object.toString());
                            OWLClass subClass = df.getOWLClass(IRI.create(subject.toString()));
                            OWLClass superClass = df.getOWLClass(IRI.create(object.toString()));
                            OWLAxiom axiom = df.getOWLSubClassOfAxiom(subClass, superClass);
                            return Collections.singleton(axiom);
                        }
                        case RDFS_SUB_PROPERTY_OF: {
                            // it means that subject and object are two object properties
                            objectProperties.add(subject.toString());
                            objectProperties.add(object.toString());
                            OWLObjectProperty subProperty = df.getOWLObjectProperty(IRI.create(subject.toString()));
                            OWLObjectProperty superProperty = df.getOWLObjectProperty(IRI.create(object.toString()));
                            OWLAxiom axiom = df.getOWLSubObjectPropertyOfAxiom(subProperty, superProperty);
                            return Collections.singleton(axiom);
                        }
                        case OWL_EQUIVALENT_PROPERTY: {
                            objectProperties.add(subject.toString());
                            objectProperties.add(object.toString());
                            OWLObjectProperty property = df.getOWLObjectProperty(subject.toString(), pm);
                            OWLObjectProperty equivProperty = df.getOWLObjectProperty(object.toString(), pm);
                            OWLAxiom axiom = df.getOWLEquivalentObjectPropertiesAxiom(property, equivProperty);
                            return Collections.singleton(axiom);
                        }

                        default:
                            String msg = "Unsupported triple predicate: " + builtinProperty;
                            logger.error(msg);
                            throw new UnsupportedOperationException(msg);
                    }

                } catch (IllegalArgumentException iae) {
                    OWLNamedIndividual individualS = df.getOWLNamedIndividual(IRI.create(subject.toString()));
                    namedIndividuals.add(subject.toString());

                    OWLObjectProperty property = df.getOWLObjectProperty(IRI.create(predicate.toString()));
                    objectProperties.add(predicate.toString());

                    OWLNamedIndividual individualO = df.getOWLNamedIndividual(IRI.create(object.toString()));
                    namedIndividuals.add(object.toString());

                    OWLAxiom axiom = df.getOWLObjectPropertyAssertionAxiom(property, individualS, individualO);
                    return Collections.singleton(axiom);
                }

            } else {
                throw new IllegalArgumentException();
            }
        } catch (Exception e) {
            logger.warn("Problem with: " + this + " in tuple " + triple);
            e.printStackTrace();
            suspendedTriples.add(triple);
            return null;
        }
    }

    private Set<OWLAxiom> convertRDFTypeTriple(Triple triple) {
        TripleNode subject = triple.getSubject();
        TripleNode predicate = triple.getPredicate();
        TripleNode object = triple.getObject();
        try {
            OWLRDFVocabulary builtinObject = OWLRDFVocabulary.valueOf(object.toString());
            switch (builtinObject) {
                case OWL_ONTOLOGY:
                    return null;
                case OWL_CLASS:
                case RDFS_CLASS: {
                    // subject is a class
                    classes.add(subject.toString());
                    OWLClass c = df.getOWLClass(IRI.create(subject.toString()));
                    return Collections.singleton(df.getOWLDeclarationAxiom(c));
                }
                case OWL_DATATYPE:
                case RDFS_DATATYPE: {
                    datatypes.add(subject.toString());
                    OWLDatatype d = df.getOWLDatatype(IRI.create(subject.toString()));
                    return Collections.singleton(df.getOWLDeclarationAxiom(d));
                }
                case OWL_OBJECT_PROPERTY: {
                    objectProperties.add(subject.toString());
                    OWLObjectProperty prop = df.getOWLObjectProperty(IRI.create(subject.toString()));
                    return Collections.singleton(df.getOWLDeclarationAxiom(prop));
                }
                case OWL_DATA_PROPERTY: {
                    dataProperties.add(subject.toString());
                    OWLDataProperty prop = df.getOWLDataProperty(IRI.create(subject.toString()));
                    return Collections.singleton(df.getOWLDeclarationAxiom(prop));
                }
                default:
                    String msg = "Unsupported type object: " + builtinObject;
                    logger.error(msg);
                    throw new UnsupportedOperationException(msg);
            }

        } catch (IllegalArgumentException iae) {
            if (isIndividual(subject)) {
                OWLClass clazz = df.getOWLClass(subject.toString(), pm);
            } else if (isClass(subject)) {
                classes.add(object.toString());
                if (configuration.getTypeBetweenClassesPolicy().equals(CONVERT_INTO_SUBCLASS)) {
                    OWLClass subClass = df.getOWLClass(IRI.create(subject.toString()));
                    OWLClass superClass = df.getOWLClass(IRI.create(object.toString()));
                    OWLAxiom axiom = df.getOWLSubClassOfAxiom(subClass, superClass);
                    return Collections.singleton(axiom);
                } else {
                    suspendedTriples.add(triple);
                    return null;
                }
            }
            if (!isClass(object) && !isIndividual(subject)) {
                suspendedTriples.add(triple);
                return null;
            } else {
                classes.add(object.toString());
                namedIndividuals.add(subject.toString());
                OWLClass clazz = df.getOWLClass(IRI.create(object.toString()));
                OWLIndividual individual = df.getOWLNamedIndividual(IRI.create(subject.toString()));
                OWLAxiom axiom = df.getOWLClassAssertionAxiom(clazz, individual);
                return Collections.singleton(axiom);
            }

        }

    }

    private boolean isClass(TripleNode node) {
        return classes.contains(node.toString());
    }

    private boolean isIndividual(TripleNode node) {
        return namedIndividuals.contains(node.toString());
    }

}
