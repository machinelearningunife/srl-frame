/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld.queue;

import it.unife.ml.kraider.api.TripleNodeBlank;
import it.unife.ml.kraider.api.TripleNodeLiteral;
import it.unife.ml.kraider.api.TripleNodeURIResource;
import it.unife.ml.kraider.api.TripleNodeVisitor;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFVisitor;
import org.apache.jena.rdf.model.Resource;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class TripleNodeConverterVisitor implements TripleNodeVisitor<OWLObject> {

    private static final Logger logger = LoggerFactory.getLogger(TripleNodeConverterVisitor.class);

    private OWLDataFactory df = OWLManager.getOWLDataFactory();

    public TripleNodeConverterVisitor() {
    }

    @Override
    public OWLObject visit(TripleNodeBlank blankNode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OWLObject visit(TripleNodeURIResource namedNode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public OWLLiteral visit(TripleNodeLiteral l) {
        if (l.isFloat()) {
            return df.getOWLLiteral(l.getFloat());
        } else if (l.isDouble()) {
            return df.getOWLLiteral(l.getDouble());
        } else if (l.isInt()) {
            return df.getOWLLiteral(l.getInt());
        } else if (l.isBoolean()) {
            return df.getOWLLiteral(l.getBoolean());
        } else if (l.isString()) {
            return df.getOWLLiteral(l.getString());
        } else {
            String msg = "strange dataytype in ontology conversion datatype: " + l.getNTripleForm();
            logger.error(msg);
            throw new IllegalArgumentException(msg);
        }
    }

}
