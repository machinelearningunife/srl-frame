/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.ld.queue;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class QueueCleanerConfiguration {

    /**
     * If there is a triple C type D, where C and D are classes, there could
     * some issues, because this triple is not correct in OWL! So it is
     * important to choose which policy must be used.
     */
    public enum TypeBetweenClassesPolicy {
        IGNORE,
        CONVERT_INTO_SUBCLASS
    }

    protected TypeBetweenClassesPolicy typeBetweenClassesPolicy;

    protected int blankNodeBufferSize;

    /**
     * Timeout of triple polling in seconds
     *
     */
    protected long triplePollTimeout;

    /**
     * General timeout.
     */
    protected long timeout;
    
    
    public QueueCleanerConfiguration(
            TypeBetweenClassesPolicy typeBetweenClassesPolicy,
            int blankNodeBufferSize,
            long triplePollTimeout,
            long timeout
            ) {
        this.typeBetweenClassesPolicy = typeBetweenClassesPolicy;
        this.blankNodeBufferSize = blankNodeBufferSize;
        this.triplePollTimeout = triplePollTimeout;
        this.timeout = timeout;
        
    }

    /**
     * @return the typeBetweenClassesPolicy
     */
    public TypeBetweenClassesPolicy getTypeBetweenClassesPolicy() {
        return typeBetweenClassesPolicy;
    }

    /**
     * @return the blankNodeBufferSize
     */
    public int getBlankNodeBufferSize() {
        return blankNodeBufferSize;
    }

    /**
     * @return the timeout
     */
    public long getTimeout() {
        return timeout;
    }

    /**
     * @return the triplePollTimeout
     */
    public long getTriplePollTimeout() {
        return triplePollTimeout;
    }

}
