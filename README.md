# SRL-Frame

## Name
SRL-Frame stands for Statistical Relational Learning Framework

## Description
SRL-Frame is a Framework for Statistical Relational Learning algorithms. It is based on the OSGi technology, therefore it allows to include new algorithms as bundles avoiding the issues relative to the use of various versions of the same dependencies (the problem known as JAR-hell or dependency hell).
